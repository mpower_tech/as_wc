(*
Bartlomiej Kucharczyk
11.06.2020
bartekkucharczyk95@gmail.com
[0] - Anemometer communication
[1] - Wind speed clear
[2] - Wind direction clear
[3] - Wind dir filtered
[4] - Average speed in period
[5] - Break state
[6] - Turbine controller communcation
[7] - Turbine controller error bits
[8] - Chopper duty
[9] - Generator speed
[10] - Turbine speed
[11] - 

*)



PROGRAM _INIT

	gStringSupport.Input.Params.NumOfSamples := 21;
	gStringSupport.Input.Params.MaxBuforLen := 9800;
	gStringSupport.Input.Params.Samples[0] := 0; // Only for tests! 1X
	
	lStringSupport.DataATime.GetData.enable := TRUE;
	lStringSupport.DataATime.GetData.pDTStructure := ADR(lStringSupport.DataATime.DTStruct);
END_PROGRAM

PROGRAM _CYCLIC
	
	CASE gStringSupport.Output.States.SM_STRING_SUPPORT OF
		SM_STRING_WAIT_FOR_ENABLE:	
			gStringSupport.Output.States.SM_STRING_SUPPORT := SM_STRING_PREPARE_SAMPLES;			
			
		SM_STRING_PREPARE_SAMPLES:
			
			IF gStringSupport.Input.Cmd.NewValue THEN
				gStringSupport.Input.Cmd.NewValue := FALSE;
				
				lStringSupport.DataATime.GetData();
							
				lStringSupport.DataATime.StringData.hour   := USINT_TO_STRING(lStringSupport.DataATime.DTStruct.hour); 
				lStringSupport.DataATime.StringData.minute := USINT_TO_STRING(lStringSupport.DataATime.DTStruct.minute);
				lStringSupport.DataATime.StringData.sec    := USINT_TO_STRING(lStringSupport.DataATime.DTStruct.second);
						
				lStringSupport.DataATime.StringData.day    := USINT_TO_STRING(lStringSupport.DataATime.DTStruct.day);
				lStringSupport.DataATime.StringData.month  := USINT_TO_STRING(lStringSupport.DataATime.DTStruct.month);
				lStringSupport.DataATime.StringData.year   := UINT_TO_STRING(lStringSupport.DataATime.DTStruct.year);
								
				//PrepareData;
				lStringSupport.DataToSave := '';
				lStringSupport.ConvSample := '';
	
				strcat(ADR(lStringSupport.DataToSave),ADR(lStringSupport.DataATime.StringData.year));
				strcat(ADR(lStringSupport.DataToSave),ADR('-'));
				strcat(ADR(lStringSupport.DataToSave),ADR(lStringSupport.DataATime.StringData.month));
				strcat(ADR(lStringSupport.DataToSave),ADR('-'));
				strcat(ADR(lStringSupport.DataToSave),ADR(lStringSupport.DataATime.StringData.day));
		
				strcat(ADR(lStringSupport.DataToSave),ADR(' '));
				strcat(ADR(lStringSupport.DataToSave),ADR(lStringSupport.DataATime.StringData.hour));
				strcat(ADR(lStringSupport.DataToSave),ADR(':'));
				strcat(ADR(lStringSupport.DataToSave),ADR(lStringSupport.DataATime.StringData.minute));
				strcat(ADR(lStringSupport.DataToSave),ADR(':'));
				strcat(ADR(lStringSupport.DataToSave),ADR(lStringSupport.DataATime.StringData.sec));
				strcat(ADR(lStringSupport.DataToSave),ADR('	'));
		
				FOR i := 0 TO gStringSupport.Input.Params.NumOfSamples-1 BY 1 DO
					lStringSupport.ConvSample := '';
					ftoa(gStringSupport.Input.Params.Samples[i],ADR(lStringSupport.ConvSample));
			
					strcat(ADR(lStringSupport.DataToSave),ADR(lStringSupport.ConvSample));
			
					IF i+1 < gStringSupport.Input.Params.NumOfSamples THEN
						strcat(ADR(lStringSupport.DataToSave),ADR('	'));
					END_IF
				END_FOR;
		
				strcat(ADR(lStringSupport.DataToSave),ADR('$n'));
				strcat(ADR(gStringSupport.Output.Status.StringToSave),ADR(lStringSupport.DataToSave)); // (!) Tutaj jest laczony string
				
				lStringSupport.LenDataToSave   := strlen(ADR(lStringSupport.DataToSave));
				lStringSupport.LenStringToSave := strlen(ADR(gStringSupport.Output.Status.StringToSave));
				gStringSupport.Output.Status.FillVector := lStringSupport.LenStringToSave;
				
				IF  (lStringSupport.LenDataToSave + lStringSupport.LenStringToSave  > 9800) OR (lStringSupport.LenStringToSave > gStringSupport.Input.Params.MaxBuforLen) THEN // (!)
					gStringSupport.Output.Status.ReadyToSave := TRUE;			
				END_IF
			END_IF
		
	END_CASE;
	
END_PROGRAM
