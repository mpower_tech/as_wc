
TYPE
	StringSupportInterType : 	STRUCT 
		DataATime : StringSupportInterDataTyp;
		DataToSave : STRING[200];
		LenStringToSave : UINT;
		LenDataToSave : UINT;
		ConvSample : STRING[100];
	END_STRUCT;
END_TYPE

(**)

TYPE
	StringSupportInterDataTyp : 	STRUCT 
		DTStruct : DTStructure;
		StringData : StringSupportInterDataStringTyp;
		GetData : DTStructureGetTime;
	END_STRUCT;
	StringSupportInterDataStringTyp : 	STRUCT 
		sec : STRING[2];
		minute : STRING[2];
		hour : STRING[2];
		day : STRING[2];
		year : STRING[4];
		month : STRING[2];
	END_STRUCT;
END_TYPE
