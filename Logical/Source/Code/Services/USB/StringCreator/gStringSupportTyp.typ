
TYPE
	StringSupportType : 	STRUCT 
		Input : StringSupportInpTyp;
		Output : StringSupportOutTyp;
	END_STRUCT;
END_TYPE

(**)

TYPE
	StringSupportInpTyp : 	STRUCT 
		Cmd : StringSupportInpCmdTyp;
		Params : StringSupportInpParTyp;
	END_STRUCT;
END_TYPE

(**)

TYPE
	StringSupportInpCmdTyp : 	STRUCT 
		NewValue : BOOL;
	END_STRUCT;
	StringSupportInpParTyp : 	STRUCT 
		MaxBuforLen : UINT; (*Max 10000 min 100*)
		Samples : ARRAY[0..40]OF REAL;
		NumOfSamples : USINT;
	END_STRUCT;
END_TYPE

(**)
(**)

TYPE
	StringSupportOutTyp : 	STRUCT 
		States : StringSupportOutStatesTyp;
		Status : StringSupportOutStatusTyp;
	END_STRUCT;
END_TYPE

(**)

TYPE
	StringSupportOutStatesTyp : 	STRUCT 
		SM_STRING_SUPPORT : SM_STRING_SUPPORT;
	END_STRUCT;
	StringSupportOutStatusTyp : 	STRUCT 
		ReadyToSave : BOOL;
		StringToSave : STRING[10000];
		Error : BOOL;
		FillVector : UINT;
	END_STRUCT;
END_TYPE

(**)

TYPE
	SM_STRING_SUPPORT : 
		(
		SM_STRING_WAIT_FOR_ENABLE,
		SM_STRING_PREPARE_SAMPLES
		);
END_TYPE
