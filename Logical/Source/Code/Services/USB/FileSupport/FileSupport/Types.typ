
TYPE
	FileSupportInterType : 	STRUCT 
		GetData : DTStructureGetTime;
		TON_SaveDepend : TON;
		TON_MaxSaveDelay : TON;
		FileNameSupport : FileSupportInterFileNameSpptTyp;
		FileSupport : FileSupportInterFileTyp;
		Offset : UDINT;
		DTStruct : DTStructure;
		FileSupportBufforData : FileSupportBufforDataTyp;
		Init : BOOL;
		Header : STRING[300];
	END_STRUCT;
END_TYPE

(**)

TYPE
	FileSupportBufforDataTyp : 	STRUCT 
		Buffor : ARRAY[0..2]OF STRING[10000];
		Index : USINT;
		FileNameIndex : ARRAY[0..2]OF STRING[80];
	END_STRUCT;
END_TYPE

(**)

TYPE
	FileSupportInterFileNameSpptTyp : 	STRUCT 
		Year : STRING[4];
		Month : STRING[2];
		InternalFileName : STRING[80];
	END_STRUCT;
END_TYPE

(**)

TYPE
	FileSupportInterFileTyp : 	STRUCT 
		FileWrite : FileWrite;
		FileOpen : FileOpen;
		FileClose : FileClose;
		FileCreate : FileCreate;
	END_STRUCT;
END_TYPE
