
PROGRAM _INIT
	gFileSupport.Output.Status.PendriveClosed := TRUE;
	gFileSupport.Input.Params.SaveTime := 30;
	
	lFileSupport.GetData.pDTStructure := ADR(lFileSupport.DTStruct);
	lFileSupport.FileSupportBufforData.Index := 0;
	lFileSupport.Init := TRUE;
	lFileSupport.Header := 'Data	Angle	Aver.speed	ControlMode	BrakingResistor	StatusCtrl	ErrorBits	Freq	L13U	L13I	L13P	L13Q	UDC_M	UDC_P	Chopper	Akt.pred.gen	Akt.pred.turb.	Rad1	Rad2	Rad3	Bateria	Rezystancja $n';
END_PROGRAM

PROGRAM _CYCLIC
	gFileSupport.Output.Status.FullVectors := lFileSupport.FileSupportBufforData.Index;
	
	CASE gFileSupport.Output.States.SM_FILE_PLC OF
		SM_FILE_PLC_WAIT_FOR_ENABLE:
			
			IF lFileSupport.FileSupportBufforData.Index <> 0 AND gDetUsb.Output.Status.DiscDetected THEN 
				IF NOT (lFileSupport.FileSupportBufforData.FileNameIndex[0] = '') THEN
					gFileSupport.Output.States.SM_FILE_PLC := SM_FILE_PLC_CHECK_FILE;	
				ELSE
					
					lFileSupport.FileSupportBufforData.FileNameIndex[0] := lFileSupport.FileSupportBufforData.FileNameIndex[1];
					lFileSupport.FileSupportBufforData.FileNameIndex[1] := lFileSupport.FileSupportBufforData.FileNameIndex[2];
					memset(ADR(lFileSupport.FileSupportBufforData.FileNameIndex[2]),0,SIZEOF(lFileSupport.FileSupportBufforData.FileNameIndex[2])); 
				
					lFileSupport.FileSupportBufforData.Buffor[0] := lFileSupport.FileSupportBufforData.Buffor[1];
					lFileSupport.FileSupportBufforData.Buffor[1] := lFileSupport.FileSupportBufforData.Buffor[2];
					memset(ADR(lFileSupport.FileSupportBufforData.Buffor[2]),0,SIZEOF(lFileSupport.FileSupportBufforData.Buffor[2])); 
				
					lFileSupport.FileSupportBufforData.Index := lFileSupport.FileSupportBufforData.Index - 1; 				
					
				END_IF
				
			END_IF
			
		SM_FILE_PLC_CHECK_FILE:
			
			lFileSupport.FileSupport.FileOpen.enable  := TRUE;
			lFileSupport.FileSupport.FileOpen.mode 	  := fiREAD_WRITE;
			lFileSupport.FileSupport.FileOpen.pDevice := ADR(gDetUsb.Output.Status.DevLink[0]); // ADR('DataSaveDevice');
			lFileSupport.FileSupport.FileOpen.pFile   := ADR(lFileSupport.FileSupportBufforData.FileNameIndex[0]);
			lFileSupport.FileSupport.FileOpen();
			
			
			IF lFileSupport.FileSupport.FileOpen.status = ERR_OK THEN
				gFileSupport.Output.States.SM_FILE_PLC 	  := SM_FILE_PLC_WRITE;
				gFileSupport.Output.Status.PendriveClosed := FALSE;
				lFileSupport.FileSupport.FileWrite.ident  := lFileSupport.FileSupport.FileOpen.ident;
				lFileSupport.FileSupport.FileWrite.offset := lFileSupport.FileSupport.FileOpen.filelen;
				
				lFileSupport.FileSupport.FileClose.ident  := lFileSupport.FileSupport.FileOpen.ident;
				gFileSupport.Output.Status.ActualFileName := gFileSupport.Input.Params.NewFileName;
				
				
			ELSIF lFileSupport.FileSupport.FileOpen.status = ERR_FUB_BUSY THEN
				;
				
			ELSIF lFileSupport.FileSupport.FileOpen.status = fiERR_FILE_NOT_FOUND  THEN
				
				gFileSupport.Output.States.SM_FILE_PLC := SM_FILE_PLC_CREATE;
				
			ELSE
				gDetUsb.Input.Cmd.ForceQuickDet := TRUE;
				gFileSupport.Output.States.SM_FILE_PLC := SM_FILE_PLC_ERROR;
			END_IF
			
		SM_FILE_PLC_CREATE:
					
			lFileSupport.FileSupport.FileCreate.enable := TRUE;
			lFileSupport.FileSupport.FileCreate.pDevice := ADR(gDetUsb.Output.Status.DevLink[0]); //ADR('DataSaveDevice');
			lFileSupport.FileSupport.FileCreate.pFile := ADR(lFileSupport.FileSupportBufforData.FileNameIndex[0]);
			lFileSupport.FileSupport.FileCreate();

			IF lFileSupport.FileSupport.FileCreate.status = ERR_OK THEN
				gFileSupport.Output.States.SM_FILE_PLC := SM_FILE_PLC_HEADER; //SM_FILE_PLC_WRITE;
				lFileSupport.FileSupport.FileWrite.ident  := lFileSupport.FileSupport.FileCreate.ident;
				lFileSupport.FileSupport.FileWrite.offset := 0;
				lFileSupport.FileSupport.FileClose.ident  := lFileSupport.FileSupport.FileCreate.ident;
				gFileSupport.Output.Status.PendriveClosed := FALSE;
				gFileSupport.Output.Status.ActualFileName := gFileSupport.Input.Params.NewFileName;
				
			ELSIF lFileSupport.FileSupport.FileCreate.status = ERR_FUB_BUSY THEN
				;
			ELSE
				gFileSupport.Output.States.SM_FILE_PLC := SM_FILE_PLC_ERROR;
			END_IF
		
		SM_FILE_PLC_HEADER:
			
			lFileSupport.FileSupport.FileWrite.enable := TRUE;
			lFileSupport.FileSupport.FileWrite.len := strlen(ADR(lFileSupport.Header));
			lFileSupport.FileSupport.FileWrite.pSrc := ADR(lFileSupport.Header);
			lFileSupport.FileSupport.FileWrite();
			
			IF lFileSupport.FileSupport.FileWrite.status = ERR_OK THEN
				gFileSupport.Output.States.SM_FILE_PLC 	  := SM_FILE_PLC_WRITE;
			
				lFileSupport.FileSupport.FileWrite.offset := strlen(ADR(lFileSupport.Header));
				lFileSupport.FileSupport.FileClose.ident  := lFileSupport.FileSupport.FileCreate.ident;
				gFileSupport.Output.Status.PendriveClosed := FALSE;	
				
			ELSIF lFileSupport.FileSupport.FileWrite.status = ERR_FUB_BUSY THEN
				;
			ELSE
				gFileSupport.Output.States.SM_FILE_PLC := SM_FILE_PLC_ERROR;
			END_IF
			
		SM_FILE_PLC_OPEN:
			
			lFileSupport.FileSupport.FileOpen.enable  := TRUE;
			lFileSupport.FileSupport.FileOpen.mode    := fiREAD_WRITE;
			lFileSupport.FileSupport.FileOpen.pDevice := ADR(gDetUsb.Output.Status.DevLink[0]);
			lFileSupport.FileSupport.FileOpen.pFile   := ADR(lFileSupport.FileSupportBufforData.FileNameIndex[0]);
			lFileSupport.FileSupport.FileOpen();
			
			IF lFileSupport.FileSupport.FileOpen.status = ERR_OK THEN
				gFileSupport.Output.States.SM_FILE_PLC := SM_FILE_PLC_WRITE;
				lFileSupport.Offset := lFileSupport.FileSupport.FileOpen.filelen;
				lFileSupport.FileSupport.FileWrite.ident := lFileSupport.FileSupport.FileOpen.ident;
				lFileSupport.FileSupport.FileClose.ident := lFileSupport.FileSupport.FileOpen.ident;
				
			ELSIF lFileSupport.FileSupport.FileOpen.status = ERR_FUB_BUSY THEN
				;
			ELSE
				gFileSupport.Output.States.SM_FILE_PLC := SM_FILE_PLC_ERROR;
			END_IF
			
		SM_FILE_PLC_WRITE:
					
			lFileSupport.FileSupport.FileWrite.enable := TRUE;
			lFileSupport.FileSupport.FileWrite.len := strlen(ADR(lFileSupport.FileSupportBufforData.Buffor[0]));
			lFileSupport.FileSupport.FileWrite.pSrc := ADR(lFileSupport.FileSupportBufforData.Buffor[0]);
			lFileSupport.FileSupport.FileWrite();
			
			IF lFileSupport.FileSupport.FileWrite.status = ERR_OK THEN
				
				lFileSupport.FileSupportBufforData.FileNameIndex[0] := lFileSupport.FileSupportBufforData.FileNameIndex[1];
				lFileSupport.FileSupportBufforData.FileNameIndex[1] := lFileSupport.FileSupportBufforData.FileNameIndex[2];
				memset(ADR(lFileSupport.FileSupportBufforData.FileNameIndex[2]),0,SIZEOF(lFileSupport.FileSupportBufforData.FileNameIndex[2])); 
				
				lFileSupport.FileSupportBufforData.Buffor[0] := lFileSupport.FileSupportBufforData.Buffor[1];
				lFileSupport.FileSupportBufforData.Buffor[1] := lFileSupport.FileSupportBufforData.Buffor[2];
				memset(ADR(lFileSupport.FileSupportBufforData.Buffor[2]),0,SIZEOF(lFileSupport.FileSupportBufforData.Buffor[2])); 
				
				lFileSupport.FileSupportBufforData.Index := lFileSupport.FileSupportBufforData.Index - 1; 
				gFileSupport.Output.Status.NumOfSave := gFileSupport.Output.Status.NumOfSave + 1;
				
				IF gFileSupport.Output.Status.NumOfSave > 4294967290 THEN
					gFileSupport.Output.Status.NumOfSave := 0;
				END_IF
				
				gFileSupport.Output.States.SM_FILE_PLC := SM_FILE_PLC_CLOSE;				
				
			ELSIF lFileSupport.FileSupport.FileWrite.status = ERR_FUB_BUSY THEN
				;
			ELSE
				gFileSupport.Output.States.SM_FILE_PLC := SM_FILE_PLC_ERROR;
			END_IF
			
		SM_FILE_PLC_CLOSE:
			lFileSupport.FileSupport.FileClose.enable := TRUE;
			lFileSupport.FileSupport.FileClose();
			
			IF lFileSupport.FileSupport.FileClose.status = ERR_OK THEN
				
				gFileSupport.Output.Status.PendriveClosed := TRUE;
				
				gFileSupport.Output.States.SM_FILE_PLC := SM_FILE_PLC_WAIT_FOR_ENABLE;
			ELSIF lFileSupport.FileSupport.FileClose.status = ERR_FUB_BUSY THEN
				;
			ELSE
				gFileSupport.Output.States.SM_FILE_PLC := SM_FILE_PLC_ERROR;		
			END_IF
					
		SM_FILE_PLC_ERROR:
			// ZROBIC WYLAPANIE BLEDU
			lFileSupport.FileSupport.FileClose.enable := FALSE;
			lFileSupport.FileSupport.FileClose();
		
			lFileSupport.FileSupport.FileCreate.enable := FALSE;
			lFileSupport.FileSupport.FileCreate();
		
			lFileSupport.FileSupport.FileOpen.enable := FALSE;
			lFileSupport.FileSupport.FileOpen();
		
			lFileSupport.FileSupport.FileWrite.enable := FALSE;
			lFileSupport.FileSupport.FileWrite();
		
			gFileSupport.Output.States.SM_FILE_PLC := SM_FILE_PLC_WAIT_FOR_ENABLE;
		
	END_CASE;
	
	IF gFileSupport.Input.Cmd.TimeDependency THEN
		lFileSupport.TON_SaveDepend.IN := TRUE;
		lFileSupport.TON_SaveDepend.PT := UINT_TO_TIME(gFileSupport.Input.Params.SaveTime * 1000);
		lFileSupport.TON_SaveDepend();
		
		IF lFileSupport.TON_SaveDepend.Q THEN
			gFileSupport.Input.Cmd.ExitPen := TRUE;
			
			lFileSupport.TON_SaveDepend.IN := FALSE;
			lFileSupport.TON_SaveDepend();
		END_IF
	ELSE
	
	END_IF
	
	// Bezpieczne wylaczenie pendrivea
	IF gFileSupport.Input.Cmd.ExitPen THEN
		gFileSupport.Input.Cmd.ExitPen := FALSE;
		
		IndexShift;
		lFileSupport.FileSupportBufforData.Buffor[lFileSupport.FileSupportBufforData.Index] := gStringSupport.Output.Status.StringToSave;
		lFileSupport.FileSupportBufforData.FileNameIndex[lFileSupport.FileSupportBufforData.Index] := gFileSupport.Input.Params.NewFileName; 
		lFileSupport.FileSupportBufforData.Index := lFileSupport.FileSupportBufforData.Index + 1;
		memset(ADR(gStringSupport.Output.Status.StringToSave),0,SIZEOF(gStringSupport.Output.Status.StringToSave)); 
		
	END_IF
	
	// Sprawdzanie zmiany daty
	CASE gFileSupport.Output.States.SM_FILE_MONTH OF
		SM_CHM_WAIT_FOR_ENABLE:
			gFileSupport.Output.States.SM_FILE_MONTH := SM_CHM_READ_DATE;
			
		SM_CHM_READ_DATE:
			// TC_1_2020.txt
			gFileSupport.Input.Params.NewFileName := 'TC_';
			
			lFileSupport.GetData.enable := TRUE;
			lFileSupport.GetData();
			
			IF lFileSupport.GetData.status = ERR_OK THEN
				
				lFileSupport.FileNameSupport.Month := USINT_TO_STRING(lFileSupport.DTStruct.month);
				lFileSupport.FileNameSupport.Year  := UINT_TO_STRING(lFileSupport.DTStruct.year);
				strcat(ADR(gFileSupport.Input.Params.NewFileName),ADR(lFileSupport.FileNameSupport.Month));
				strcat(ADR(gFileSupport.Input.Params.NewFileName),ADR('_'));
				strcat(ADR(gFileSupport.Input.Params.NewFileName),ADR(lFileSupport.FileNameSupport.Year));
				strcat(ADR(gFileSupport.Input.Params.NewFileName),ADR('.txt'));
				
				IF NOT (gFileSupport.Input.Params.NewFileName = gFileSupport.Output.Status.ActualFileName) THEN
					IndexShift;
					lFileSupport.FileSupportBufforData.Buffor[lFileSupport.FileSupportBufforData.Index] := gStringSupport.Output.Status.StringToSave;
					lFileSupport.FileSupportBufforData.FileNameIndex[lFileSupport.FileSupportBufforData.Index] := gFileSupport.Output.Status.ActualFileName;
					lFileSupport.FileSupportBufforData.Index := lFileSupport.FileSupportBufforData.Index + 1;
					memset(ADR(gStringSupport.Output.Status.StringToSave),0,SIZEOF(gStringSupport.Output.Status.StringToSave)); 	
					gFileSupport.Output.Status.ActualFileName := gFileSupport.Input.Params.NewFileName;
				END_IF
				
			ELSIF lFileSupport.GetData.status = ERR_FUB_BUSY THEN
				;
			ELSE
				gFileSupport.Output.States.SM_FILE_MONTH := SM_CHM_ERROR;
			END_IF
			
		SM_CHM_ERROR:
		
			lFileSupport.GetData.enable := FALSE;
			lFileSupport.GetData();
			gFileSupport.Output.States.SM_FILE_MONTH := SM_CHM_WAIT_FOR_ENABLE;
		
	END_CASE;
	
	// Sprawdzanie statusu gotowosci do zapisu
	CASE gFileSupport.Output.States.SM_FILE_RDY_DATA OF
		SM_DRD_WAIT:
			gFileSupport.Output.States.SM_FILE_RDY_DATA := SM_DRD_CHECK_LEN;
		SM_DRD_CHECK_LEN:
			IF gStringSupport.Output.Status.ReadyToSave THEN
				gStringSupport.Output.Status.ReadyToSave := FALSE;
				IndexShift;
				lFileSupport.FileSupportBufforData.Buffor[lFileSupport.FileSupportBufforData.Index] := gStringSupport.Output.Status.StringToSave;
				lFileSupport.FileSupportBufforData.FileNameIndex[lFileSupport.FileSupportBufforData.Index] := gFileSupport.Input.Params.NewFileName; 
				lFileSupport.FileSupportBufforData.Index := lFileSupport.FileSupportBufforData.Index + 1;
				memset(ADR(gStringSupport.Output.Status.StringToSave),0,SIZEOF(gStringSupport.Output.Status.StringToSave)); 
			END_IF;		  

	END_CASE;
	
	lFileSupport.Init := FALSE;
END_PROGRAM

