
TYPE
	FileSupportType : 	STRUCT 
		Input : FileSupportInpTyp;
		Output : FileSupportOutTyp;
	END_STRUCT;
END_TYPE

(**)

TYPE
	FileSupportInpTyp : 	STRUCT 
		Cmd : FileSupportInpCmdTyp;
		Params : FileSupportInpParTyp;
	END_STRUCT;
END_TYPE

(**)

TYPE
	FileSupportInpCmdTyp : 	STRUCT 
		ExitPen : BOOL;
		TimeDependency : BOOL;
	END_STRUCT;
	FileSupportInpParTyp : 	STRUCT 
		NewFileName : STRING[80];
		SaveTime : UINT;
	END_STRUCT;
END_TYPE

(**)

TYPE
	FileSupportOutTyp : 	STRUCT 
		States : FileSupportOutStatesTyp;
		Status : FileSupportOutStatusTyp;
	END_STRUCT;
END_TYPE

(**)

TYPE
	FileSupportOutStatesTyp : 	STRUCT 
		SM_FILE_PLC : SM_FILE_PLC;
		SM_FILE_MONTH : SM_CHECK_MONTH;
		SM_FILE_RDY_DATA : SM_DETECT_RDY_DATA;
	END_STRUCT;
	FileSupportOutStatusTyp : 	STRUCT 
		Error : BOOL;
		Sended : BOOL;
		ActualFileName : STRING[80];
		PendriveClosed : BOOL;
		NumOfSave : UDINT;
		FullVectors : USINT;
	END_STRUCT;
END_TYPE

(**)

TYPE
	SM_FILE_PLC : 
		(
		SM_FILE_PLC_WAIT_FOR_ENABLE,
		SM_FILE_PLC_CHECK_DATA,
		SM_FILE_PLC_CHECK_FILE,
		SM_FILE_PLC_OPEN,
		SM_FILE_PLC_READ,
		SM_FILE_PLC_WRITE,
		SM_FILE_PLC_CLOSE,
		SM_FILE_PLC_WAIT,
		SM_FILE_PLC_ERROR,
		SM_FILE_PLC_DELETE,
		SM_FILE_PLC_CREATE,
		SM_FILE_PLC_HEADER
		);
END_TYPE

(**)

TYPE
	SM_CHECK_MONTH : 
		(
		SM_CHM_WAIT_FOR_ENABLE,
		SM_CHM_READ_DATE,
		SM_CHM_ERROR
		);
END_TYPE

(**)

TYPE
	SM_DETECT_RDY_DATA : 
		(
		SM_DRD_WAIT,
		SM_DRD_CHECK_LEN
		);
END_TYPE

(**)
