
TYPE
	DetUsbType : 	STRUCT 
		Input : DetUsbInpTyp;
		Output : DetUsbOutTyp;
	END_STRUCT;
END_TYPE

(**)

TYPE
	DetUsbInpTyp : 	STRUCT 
		Cmd : DetUsbInpCmdTyp;
		Params : DetUsbInpParTyp;
	END_STRUCT;
END_TYPE

(**)

TYPE
	DetUsbInpCmdTyp : 	STRUCT 
		Enable : BOOL; (*Start detection pendrive*)
		Reset : BOOL; (*Reset error*)
		Stop : BOOL; (*Stop detection pendrive*)
		ForceQuickDet : BOOL; (*Not wait! Detect now!*)
	END_STRUCT;
	DetUsbInpParTyp : 	STRUCT 
		AutoDetectionTime : TIME; (*Time to again detection*)
	END_STRUCT;
END_TYPE

(**)
(**)

TYPE
	DetUsbOutTyp : 	STRUCT 
		States : DetUsbOutStatesTyp;
		Status : DetUsbOutStatusTyp;
	END_STRUCT;
END_TYPE

(**)

TYPE
	DetUsbOutStatesTyp : 	STRUCT 
		SM_DET_USB : USB_DET_SM; (*State machine*)
	END_STRUCT;
	DetUsbOutStatusTyp : 	STRUCT 
		DevHandle : UDINT;
		DevLink : ARRAY[0..9]OF STRING[50];
		DiscDetected : BOOL; (*Pendrive is detected*)
		Error : BOOL; (*Error in state machine*)
		NumOfDev : USINT; (*Number of detected pendrives*)
		Internal : DetUsbOutStatusInterTyp; (*Internal information*)
	END_STRUCT;
	DetUsbOutStatusInterTyp : 	STRUCT 
		Index : USINT; (*Index*)
		Time : USINT; (*Time of not detected pendrive*)
		NumOfError : INT; (*Number of errors*)
	END_STRUCT;
END_TYPE

(**)
(**)

TYPE
	USB_DET_SM : 
		(
		USB_DET_INIT,
		USB_DET_GET_NODE_LIST,
		USB_DET_GET_NODE_INFO,
		USB_DET_DELETE_DEV_LINK,
		USB_DET_QUICK_DETECTION,
		USB_DET_CREATE_DEV_LINK,
		USB_DET_WAIT,
		USB_DET_ERROR
		);
END_TYPE
