
TYPE
	DetUsbInternalType : 	STRUCT 
		DataInternal : DetUsbInterDataInterTyp;
		UsbNodeGet : UsbNodeGet;
		DevUnlink : DevUnlink;
		DevLink : DevLink;
		UsbMsDeviceReady : UsbMsDeviceReady;
		UsbNodeListGet : UsbNodeListGet;
		Redetection : TON;
	END_STRUCT;
	DetUsbInterDataInterTyp : 	STRUCT 
		pParam : STRING[80];
		NodeIfBuffer : ARRAY[0..9]OF UDINT;
		UsbData : usbNode_typ;
	END_STRUCT;
END_TYPE
