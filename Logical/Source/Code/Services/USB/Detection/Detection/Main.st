(*
Author: Bartlomiej Kucharczy
Date: 15.12.2019
Program: Task automatically search mass storage.
Glowne zalozenie: Pendrive bedzie wsadzony i trzymany przez dlugi czas.

TODO:
- generowanie alarmu
- 
*)


PROGRAM _INIT
	(* NodeListGet init *)
	lDetUsbInternal.UsbNodeListGet.filterInterfaceClass    := 8;
	lDetUsbInternal.UsbNodeListGet.filterInterfaceSubClass := 0;
	
	lDetUsbInternal.UsbNodeListGet.pBuffer := ADR(lDetUsbInternal.DataInternal.NodeIfBuffer);
	lDetUsbInternal.UsbNodeListGet.bufferSize := SIZEOF(lDetUsbInternal.DataInternal.NodeIfBuffer);
	
	lDetUsbInternal.UsbNodeGet.pBuffer := ADR(lDetUsbInternal.DataInternal.UsbData);
	lDetUsbInternal.UsbNodeGet.bufferSize := SIZEOF(lDetUsbInternal.DataInternal.UsbData);
	
	lDetUsbInternal.DevLink.pParam := ADR(lDetUsbInternal.DataInternal.pParam);
	
	gDetUsb.Input.Params.AutoDetectionTime := T#30s;
	
END_PROGRAM

PROGRAM _CYCLIC
	
	CASE gDetUsb.Output.States.SM_DET_USB OF
		USB_DET_INIT: // Inicjacja i czekanie  na enable	
			gDetUsb.Input.Cmd.Enable := TRUE;
			IF gDetUsb.Input.Cmd.Enable THEN
				gDetUsb.Output.States.SM_DET_USB := USB_DET_GET_NODE_LIST;
				memset(ADR(gDetUsb.Output.Status.DevLink), 0, SIZEOF(gDetUsb.Output.Status.DevLink));
				gDetUsb.Output.Status.DiscDetected := FALSE;
				gDetUsb.Output.Status.NumOfDev := 0;
			END_IF
			
			
		USB_DET_GET_NODE_LIST: // Szukaj pendrive'a
			
			
			lDetUsbInternal.UsbNodeListGet.enable := TRUE;
			lDetUsbInternal.UsbNodeListGet();
			gDetUsb.Output.Status.NumOfDev := UDINT_TO_USINT(lDetUsbInternal.UsbNodeListGet.allNodes);
			
			IF lDetUsbInternal.UsbNodeListGet.status = ERR_OK THEN
				// Okey, it look that something was detected
				
				IF lDetUsbInternal.UsbNodeListGet.allNodes > 0 THEN
					gDetUsb.Output.States.SM_DET_USB := USB_DET_GET_NODE_INFO;
					gDetUsb.Output.Status.DiscDetected := TRUE;
				END_IF
				
			ELSIF lDetUsbInternal.UsbNodeListGet.status = asusbERR_USB_NOTFOUND THEN
				; // Do it again, until detection
				
			ELSIF lDetUsbInternal.UsbNodeListGet.status = ERR_FUB_BUSY THEN
				; // Wait for end				
				
			ELSE
				lDetUsbInternal.UsbNodeListGet.enable := FALSE;
				lDetUsbInternal.UsbNodeListGet();
				gDetUsb.Output.States.SM_DET_USB := USB_DET_ERROR;
			END_IF
				
			
		USB_DET_GET_NODE_INFO: // Pobierz dane o pendrive
			
			
			lDetUsbInternal.UsbNodeGet.enable := TRUE;
			lDetUsbInternal.UsbNodeGet.nodeId := lDetUsbInternal.DataInternal.NodeIfBuffer[0]; // Only first ID Buffer!!
			lDetUsbInternal.UsbNodeGet();
			
			IF lDetUsbInternal.UsbNodeGet.status = ERR_OK THEN
				// Okey, information was loaded			
				
				// Create the name of the file device 
				gDetUsb.Output.Status.DevLink[0] := 'USB: ';
				strcat(ADR(gDetUsb.Output.Status.DevLink[0]), ADR(lDetUsbInternal.DataInternal.UsbData.ifName));
				
				// Create pParam for DevLink FB
				lDetUsbInternal.DataInternal.pParam := '/DEVICE=';
				strcat(ADR(lDetUsbInternal.DataInternal.pParam), ADR(lDetUsbInternal.DataInternal.UsbData.ifName)); 
				
				lDetUsbInternal.UsbNodeGet.enable := FALSE;
				lDetUsbInternal.UsbNodeGet();
				gDetUsb.Output.States.SM_DET_USB := USB_DET_CREATE_DEV_LINK;
				
			ELSIF lDetUsbInternal.UsbNodeGet.status = asusbERR_USB_NOTFOUND THEN
				// Come back to detection!
				lDetUsbInternal.UsbNodeGet.enable := FALSE;
				lDetUsbInternal.UsbNodeGet();
				gDetUsb.Output.States.SM_DET_USB := USB_DET_INIT;
				
			ELSIF lDetUsbInternal.UsbNodeGet.status = ERR_FUB_BUSY THEN
				; // Wait for end				
				
			ELSE
				lDetUsbInternal.UsbNodeGet.enable := FALSE;
				lDetUsbInternal.UsbNodeGet();
				gDetUsb.Output.States.SM_DET_USB := USB_DET_ERROR;
			END_IF
			
			
		USB_DET_CREATE_DEV_LINK: // Utworz dev link
		
			
			lDetUsbInternal.DevLink.pDevice := ADR(gDetUsb.Output.Status.DevLink[0]);
			lDetUsbInternal.DevLink.enable  := TRUE;
			lDetUsbInternal.DevLink();
			
			IF lDetUsbInternal.DevLink.status = ERR_OK THEN
				gDetUsb.Output.Status.DevHandle := lDetUsbInternal.DevLink.handle;
				
				lDetUsbInternal.Redetection.PT := gDetUsb.Input.Params.AutoDetectionTime;
				gDetUsb.Output.States.SM_DET_USB := USB_DET_WAIT;
			ELSIF lDetUsbInternal.DevLink.status = ERR_FUB_BUSY THEN
				;
			ELSE 
				gDetUsb.Output.States.SM_DET_USB := USB_DET_ERROR;
			END_IF
			
			
		USB_DET_WAIT: // czekaj
			// Ponowne proba wykrycia po 10 min
				
			lDetUsbInternal.Redetection.IN := TRUE;
			lDetUsbInternal.Redetection();
			
			IF lDetUsbInternal.Redetection.Q  OR gDetUsb.Input.Cmd.ForceQuickDet THEN // OR ERROR IN SAVE FILES ON MASS STORAGE
				
				lDetUsbInternal.Redetection.IN := FALSE;
				lDetUsbInternal.Redetection();
				gDetUsb.Input.Cmd.ForceQuickDet := FALSE;
				gDetUsb.Output.States.SM_DET_USB := USB_DET_QUICK_DETECTION;
			END_IF				
			
		USB_DET_QUICK_DETECTION: // Szybkie sprawdzenie czy jest pendrive
			
			lDetUsbInternal.UsbMsDeviceReady.enable := TRUE;
			lDetUsbInternal.UsbMsDeviceReady.pIfName := ADR(lDetUsbInternal.DataInternal.UsbData.ifName);
			lDetUsbInternal.UsbMsDeviceReady();
			
			IF lDetUsbInternal.UsbMsDeviceReady.status = ERR_OK THEN // Pen is okey!
				
				lDetUsbInternal.Redetection.PT := gDetUsb.Input.Params.AutoDetectionTime;
				gDetUsb.Output.Status.DiscDetected := TRUE;
				lDetUsbInternal.UsbMsDeviceReady.enable := FALSE;
				lDetUsbInternal.UsbMsDeviceReady();
				gDetUsb.Output.States.SM_DET_USB := USB_DET_WAIT;
				
			ELSIF lDetUsbInternal.UsbMsDeviceReady.status =  asusbERR_USB_NOTFOUND THEN
				
				gDetUsb.Output.Status.DiscDetected := FALSE;
				lDetUsbInternal.UsbMsDeviceReady.enable := FALSE;
				lDetUsbInternal.UsbMsDeviceReady();
				gDetUsb.Output.States.SM_DET_USB := USB_DET_DELETE_DEV_LINK;
				
			ELSIF lDetUsbInternal.UsbMsDeviceReady.status = ERR_FUB_BUSY THEN
				;			
				
			ELSE
				
				gDetUsb.Output.States.SM_DET_USB := USB_DET_ERROR;
				
			END_IF
				
				
		USB_DET_DELETE_DEV_LINK: // kasacja devlinka
		
			lDetUsbInternal.DevUnlink.enable := TRUE;
			lDetUsbInternal.DevUnlink.handle := lDetUsbInternal.DevLink.handle;
			lDetUsbInternal.DevUnlink();
			
			IF lDetUsbInternal.DevUnlink.status = ERR_OK THEN
				
				lDetUsbInternal.DevUnlink.enable := FALSE;
				lDetUsbInternal.DevUnlink();
				gDetUsb.Output.States.SM_DET_USB := USB_DET_INIT;
			
			ELSIF lDetUsbInternal.DevUnlink.status = ERR_FUB_BUSY THEN
				;
				
			ELSE
				lDetUsbInternal.DevUnlink.enable := FALSE;
				lDetUsbInternal.DevUnlink();
				gDetUsb.Output.States.SM_DET_USB := USB_DET_ERROR;
			END_IF
				
		
		USB_DET_ERROR: // blad blokow funkcyjnych
			gDetUsb.Output.Status.Error := TRUE;
			
			IF gDetUsb.Input.Cmd.Reset THEN
				gDetUsb.Input.Cmd.Reset := FALSE;
				gDetUsb.Output.Status.Error := FALSE;
				gDetUsb.Output.States.SM_DET_USB := USB_DET_INIT;
			END_IF
				
	END_CASE;
	
	 
END_PROGRAM
