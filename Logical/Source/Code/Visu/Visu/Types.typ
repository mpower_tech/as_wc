
TYPE
	VisuTyp : 	STRUCT 
		Dialog : USINT;
		Layers : VisuLayersTyp;
		Button : VisuBtnTyp;
		Scale : VisuScaleTyp;
		Numeric : VisuNumericTyp;
		Blokade : VisuBlokadeTyp;
	END_STRUCT;
END_TYPE

(**)

TYPE
	VisuLayersTyp : 	STRUCT 
		Pumps : INT;
		Heater : INT;
	END_STRUCT;
END_TYPE

(**)

TYPE
	VisuScaleTyp : 	STRUCT 
		thermalPowerScale : REAL;
		flowScale : REAL;
		frequencyScale : REAL;
		tempScale : REAL;
		tempMinScale : REAL;
	END_STRUCT;
END_TYPE

(**)

TYPE
	VisuBtnTyp : 	STRUCT 
		PumpTurnOn : INT;
		CoolerMachine : INT;
		Heater : INT;
		ConstValueStart : INT;
	END_STRUCT;
END_TYPE

(**)

TYPE
	VisuNumericTyp : 	STRUCT 
		PercVoltage : INT;
		FlowSet : INT;
	END_STRUCT;
END_TYPE

(**)

TYPE
	VisuBlokadeTyp : 	STRUCT 
		Auto : INT;
		Manual : INT;
	END_STRUCT;
END_TYPE
