
PROGRAM _INIT
	
	gExtDev.TempControl.histeresis := 2.0;
	 
END_PROGRAM

PROGRAM _CYCLIC
	(*
	Cel: Osiągnięcie i utrzymanie temperatury na zadanym poziomie.
	
	Temp za mala: Wlacz grzalke
	Temp za duza: Wylacz grzalke
	
	Temp + histeraza za duza: wlacz chlodzenie
	Temp + histereza za mala: wlacz grzalke
	
	Histeraza = +- 3 st. C
	Mcpw - 10 s
	
	Mcpw - Minimalny Czas Pomiedzy Wlaczeniami
	
	 0 : WAIT FOR START
	 1 : CHOICE: HEATING OR COOLING
	 2 : CHECK THE HISTERESIS REQ.
	 3 : HEATING: WAIT FOR: ACT. >   
	
		*)
	CASE gExtDev.TempControl.SM OF
		0:
			IF gExtDev.TempControl.start THEN
				gExtDev.TempControl.SM := 1;
			END_IF
				
		
		1:
				
			// TEMP TANK < SET TEMP TANK //
			IF gExtDev.TempMeter.Tank.temp < gExtDev.TempControl.setTankTemp THEN
				gExtDev.TempControl.SM := 2;
							
				// TEMP TANK > SET TEMP TANK //
			ELSE
				gExtDev.TempControl.SM := 10; // COOLING
						
			END_IF
			
			
		2:
		  
			// HISTERESIS CHECK
			IF ABS(gExtDev.TempMeter.Tank.temp - gExtDev.TempControl.setTankTemp) > gExtDev.TempControl.histeresis THEN
				// IF DIFFERENCE IS BIGGER THAN HISTERESIS TURN ON THE HEATER
				gExtDev.TempControl.SM := 3;
			ELSE 
				gExtDev.TempControl.SM := 1;						
			END_IF
			
		
		3: // HEATING
			gExtDev.Heater := TRUE;
		
			IF gExtDev.TempControl.setTankTemp < (gExtDev.TempMeter.Tank.temp * 1.05) THEN
				
				gExtDev.Heater := FALSE;
				gExtDev.TempControl.SM := 1;
				
			END_IF		

		
		10:
			// HISTERESIS CHECK
			IF (gExtDev.TempMeter.Tank.temp - gExtDev.TempControl.setTankTemp) > gExtDev.TempControl.histeresis THEN
				// IF DIFFERENCE IS BIGGER THAN HISTERESIS TURN ON THE HEATER
				gExtDev.TempControl.SM := 11;
			ELSE 
				gExtDev.TempControl.SM := 1;						
			END_IF
		
		11:
			gExtDev.WaterPumpCoolerAndFan := TRUE;
		
			IF gExtDev.TempControl.setTankTemp >= gExtDev.TempMeter.Tank.temp THEN
				
				gExtDev.WaterPumpCoolerAndFan := FALSE;
				gExtDev.TempControl.SM := 1;
				
			END_IF	
	END_CASE;
	
	
	
	
	
	// TURN OFF HEATER AND COOLER PUMP
	IF EDGENEG(gExtDev.TempControl.start) THEN
		
		gExtDev.Heater := FALSE;
		gExtDev.WaterPumpCoolerAndFan := FALSE;	
		gExtDev.TempControl.SM := 0;
	END_IF

	
END_PROGRAM



