PROGRAM _INIT
	(* PID config *)
	PIDFubs.Enable := TRUE;			
	PIDFubs.MaxOut := 16384;
	PIDFubs.MinOut := 0;
	
	PIDParams.Gain := 20;
	PIDParams.IntegrationTime := 0.1;
	PIDParams.DerivativeTime := 0;
	PIDParams.FilterTime := 0;

	PIDFubs.PIDParameters := PIDParams;
	
END_PROGRAM

PROGRAM _CYCLIC
	
	// Waterpump	
	IF NOT gExtDev.WaterPump.start THEN	
		gExtDev.WaterPump.analogOutput := 0;	
		PIDFubs.Enable  := FALSE;
		
	ELSE
		// Manual mode
		IF gExtDev.WaterPump.mode = 1 THEN
			gExtDev.WaterPump.analogOutput := REAL_TO_INT(gExtDev.WaterPump.setPerc * 163.84);
			
		// Auto mode
		ELSIF gExtDev.WaterPump.mode = 2 THEN
			
			// PID Block  
			PIDFubs.ActValue := gExtDev.Flowmeter.flow;
			PIDFubs.SetValue := gExtDev.WaterPump.setFlow;
			PIDFubs.Enable  := TRUE;
			
			gExtDev.WaterPump.analogOutput := REAL_TO_INT(PIDFubs.Out);
			
		// None mode
		ELSE
			PIDFubs.Enable  := FALSE;
			gExtDev.WaterPump.analogOutput := 0;	
			gExtDev.WaterPump.start := FALSE;	
		END_IF
			
	END_IF
	
	
	IF PIDFubs.UpdateDone THEN
		PIDFubs.Update := FALSE;
	END_IF
	
	gExtDev.WaterPump.voltage := gExtDev.WaterPump.analogOutput / 163.84 * 0.24;
	
	PIDFubs();
	 
END_PROGRAM
