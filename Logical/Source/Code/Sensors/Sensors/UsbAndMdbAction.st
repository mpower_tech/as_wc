
ACTION UsbAndModbus: 

	// USB Save
	gStringSupport.Input.Params.NumOfSamples := 6;
	gStringSupport.Input.Params.Samples[0]   := gExtDev.TempMeter.Inlet.temp;
	gStringSupport.Input.Params.Samples[1]   := gExtDev.TempMeter.Outlet.temp;
	gStringSupport.Input.Params.Samples[2]   := gExtDev.TempMeter.Tank.temp;
	gStringSupport.Input.Params.Samples[3]   := gExtDev.Flowmeter.flow;
	gStringSupport.Input.Params.Samples[4]   := gExtDev.PowerThermal.powerTerm;
	gStringSupport.Input.Params.Samples[5]	 := gExtDev.WaterPump.setPerc;
	gStringSupport.Input.Cmd.NewValue := TRUE;
	
	// Modbus Output
	gExtDev.ModbusOut.actFlow      := REAL_TO_UINT(gExtDev.Flowmeter.flow*100);
	gExtDev.ModbusOut.powerThermal := REAL_TO_UINT(gExtDev.PowerThermal.powerTerm*100);
	gExtDev.ModbusOut.tempInlet    := DINT_TO_UINT(gExtDev.TempMeter.Inlet.temp0x);
	gExtDev.ModbusOut.tempOutlet   := DINT_TO_UINT(gExtDev.TempMeter.Outlet.temp0x);
	gExtDev.ModbusOut.tempTank     := DINT_TO_UINT(gExtDev.TempMeter.Tank.temp0x);
	
	
END_ACTION
