
ACTION Flow_measurement: 
	
	// Get pulses and calculate frequency //
	IF gExtDev.Flowmeter.pulses = 65535 OR gExtDev.Flowmeter.pulses = 0 THEN
		gExtDev.Flowmeter.frequency := 0;
	ELSE
		gExtDev.Flowmeter.frequency := ((1/(gExtDev.Flowmeter.pulses / 187500.0))/2.0);
	END_IF			
		
	// Flow // 		
	gExtDev.Flowmeter.flow := gExtDev.Flowmeter.frequency * (60.0/294);	// 202.5 - calibarion! 294
	
	MovingAverageFiowMeter.In := gExtDev.Flowmeter.flow;
	MovingAverageFiowMeter();
	
	gExtDev.Flowmeter.flow := MovingAverageFiowMeter.Out;
	
END_ACTION
