
ACTION Thermal_measurement: 

	// Obliczanie mocy termicznej
	// https://www.naukowiec.org/wzory/inzynieria/znamionowa-moc-cieplna_2212.html 
	// ^ Link do zrodla wzoru
	// srednie cieplo wlasciwe wody [kJ/kg*K]
	
	gExtDev.PowerThermal.powerTerm := gExtDev.Flowmeter.flow / 60.0 * 4189.9 * (gExtDev.TempMeter.Inlet.temp - gExtDev.TempMeter.Outlet.temp);	
	
END_ACTION
