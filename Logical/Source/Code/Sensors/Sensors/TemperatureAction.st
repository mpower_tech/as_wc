
ACTION Temperature_measurement: 

	
	// Inlet //
	IF gExtDev.TempMeter.Inlet.mainState.0 = 0 AND gExtDev.TempMeter.Inlet.mainState.1 = 0 THEN		
		gExtDev.TempMeter.Inlet.state := TRUE;
	ELSE
		gExtDev.TempMeter.Inlet.state := FALSE;
	END_IF
	
	// Outlet //
	IF gExtDev.TempMeter.Inlet.mainState.2 = 0 AND gExtDev.TempMeter.Inlet.mainState.3 = 0 THEN	
		gExtDev.TempMeter.Outlet.state := TRUE;	
	ELSE
		gExtDev.TempMeter.Outlet.state := FALSE;	
	END_IF
	
	// Tank //
	IF gExtDev.TempMeter.Inlet.mainState.4 = 0 AND gExtDev.TempMeter.Inlet.mainState.5 = 0 THEN	
		gExtDev.TempMeter.Tank.state := TRUE;	
	ELSE
		gExtDev.TempMeter.Tank.state := FALSE;	
	END_IF
	
	// Temperature convert //
	IF gExtDev.TempMeter.Inlet.state THEN
		gExtDev.TempMeter.Inlet.temp  := DINT_TO_REAL(gExtDev.TempMeter.Inlet.temp0x) / 100.0;
	ELSE
		gExtDev.TempMeter.Inlet.temp  := 0.0;
	END_IF	
	
	IF gExtDev.TempMeter.Outlet.state THEN
		gExtDev.TempMeter.Outlet.temp  := DINT_TO_REAL(gExtDev.TempMeter.Outlet.temp0x) / 100.0;
	ELSE
		gExtDev.TempMeter.Outlet.temp  := 0.0;
	END_IF	
	
	IF gExtDev.TempMeter.Tank.state THEN
		gExtDev.TempMeter.Tank.temp  := DINT_TO_REAL(gExtDev.TempMeter.Tank.temp0x) / 100.0;
	ELSE
		gExtDev.TempMeter.Tank.temp  := 0.0;
	END_IF	
	
END_ACTION
