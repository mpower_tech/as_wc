PROGRAM _INIT

	MovingAverageFiowMeter.WindowLength := 6;
	MovingAverageFiowMeter.Enable := TRUE;
	
END_PROGRAM
      
PROGRAM _CYCLIC
		
	
	(* STEP 1: TEMPERATURE MEASUREMENT *)
	Temperature_measurement;
	

	(* STEP 2: FLOW MEASUREMENT *)
	Flow_measurement;
	
	
	(* STEP 3: THERMAL POWER MEASUREMENT *)
	Thermal_measurement;
	
	
	(* STEP 4: USB SAVE & MODBUS *)	
	UsbAndModbus;
	
END_PROGRAM


