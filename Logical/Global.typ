
TYPE
	ExtDevType : 	STRUCT 
		TempMeter : ExtTempmeterTyp;
		Flowmeter : ExtDevFlowmeterTyp;
		WaterPump : ExtDevWaterpumpTyp;
		WaterPumpCoolerAndFan : BOOL;
		Heater : BOOL;
		Modules : ExtDevModulesTyp;
		ModbusOut : ExtDevModbusOutTyp;
		PowerThermal : ExtPowerThermalTyp;
		TempControl : ExtDevTempControlTyp;
	END_STRUCT;
END_TYPE

(**)
(*Tempmeter*)

TYPE
	ExtTempmeterTyp : 	STRUCT 
		Inlet : ExtDevTempTyp;
		Outlet : ExtDevTempTyp;
		Tank : ExtDevTempTyp;
	END_STRUCT;
END_TYPE

(**)
(*PT 100*)

TYPE
	ExtDevTempTyp : 	STRUCT 
		state : BOOL;
		temp : REAL;
		temp0x : DINT;
		mainState : USINT;
	END_STRUCT;
END_TYPE

(**)
(**)
(*ThermalPower*)

TYPE
	ExtPowerThermalTyp : 	STRUCT 
		powerTerm : REAL;
	END_STRUCT;
END_TYPE

(**)
(*Flowmeter*)

TYPE
	ExtDevFlowmeterTyp : 	STRUCT 
		pulses : UINT := 0; (*Act pulses*)
		flow : REAL := 0;
		frequency : REAL;
	END_STRUCT;
END_TYPE

(**)
(*Water pump*)

TYPE
	ExtDevWaterpumpTyp : 	STRUCT 
		mode : USINT; (*0 - None / 1 - Manual / 2- Auto*)
		start : BOOL;
		setPerc : USINT;
		setFlow : REAL;
		analogOutput : INT;
		voltage : REAL;
	END_STRUCT;
END_TYPE

(**)
(*Modules state*)

TYPE
	ExtDevModulesTyp : 	STRUCT 
		ATB312 : BOOL;
		CM8281 : BOOL;
		AO4622 : BOOL;
	END_STRUCT;
	ExtDevModbusOutTyp : 	STRUCT 
		actFlow : UINT;
		powerThermal : UINT;
		tempInlet : UINT;
		tempOutlet : UINT;
		tempTank : UINT;
	END_STRUCT;
END_TYPE

(**)

TYPE
	ExtDevTempControlTyp : 	STRUCT 
		setTankTemp : REAL;
		start : BOOL;
		histeresis : REAL;
		SM : USINT;
	END_STRUCT;
END_TYPE
