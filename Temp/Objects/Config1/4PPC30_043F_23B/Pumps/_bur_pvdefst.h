#ifndef __AS__TYPE_MTPIDParametersType
#define __AS__TYPE_MTPIDParametersType
typedef struct MTPIDParametersType
{	float Gain;
	float IntegrationTime;
	float DerivativeTime;
	float FilterTime;
} MTPIDParametersType;
#endif

#ifndef __AS__TYPE_MTPIDIntegrationEnum
#define __AS__TYPE_MTPIDIntegrationEnum
typedef enum MTPIDIntegrationEnum
{	mtINTEGRATION_FREE = 0,
	mtHOLD_INTEGRATION_POSITIVE = 1,
	mtHOLD_INTEGRATION_NEGATIVE = -1,
} MTPIDIntegrationEnum;
#endif

#ifndef __AS__TYPE_MTCommType
#define __AS__TYPE_MTCommType
typedef struct MTCommType
{	signed long ID;
	plcbit Valid;
	unsigned long Counter;
} MTCommType;
#endif

#ifndef __AS__TYPE_MTTransferFcnType
#define __AS__TYPE_MTTransferFcnType
typedef struct MTTransferFcnType
{	MTCommType Communication;
	float Numerator[6];
	float Denominator[6];
	float SampleTime;
	signed long CheckID;
} MTTransferFcnType;
#endif

#ifndef __AS__TYPE_MTBasicsPIDInternalType
#define __AS__TYPE_MTBasicsPIDInternalType
typedef struct MTBasicsPIDInternalType
{	float CycleTime;
	plcbit ParametersValid;
	MTPIDParametersType PIDParameters;
	float MinOut;
	float MaxOut;
	plcbit Invert;
	float ControlError;
	float ControlErrorOld;
	float Out;
	MTPIDIntegrationEnum HoldIntegration;
	float IntegrationPartPresetValue;
	plcbit SetIntegrationPart;
	plcbit SetIntegrationPartOld;
	plcbit EnableTrackingOld;
	signed long PID_StatusID;
	plcbit UpdateOld;
	plcbit EnableOld;
	plcbit EnableDone;
	plcbit DisabledBusy;
	unsigned long CounterOld;
	plcbit SysRefParaNew;
	MTTransferFcnType SystemReference;
} MTBasicsPIDInternalType;
#endif

struct MTBasicsPID
{	MTPIDParametersType PIDParameters;
	float MinOut;
	float MaxOut;
	float SetValue;
	float ActValue;
	float IntegrationPartPresetValue;
	float TrackingValue;
	MTPIDIntegrationEnum HoldIntegration;
	signed long StatusID;
	float Out;
	float ControlError;
	float ProportionalPart;
	float IntegrationPart;
	float DerivativePart;
	MTPIDIntegrationEnum IntegrationStatus;
	unsigned long SystemReference;
	MTBasicsPIDInternalType Internal;
	plcbit Enable;
	plcbit Invert;
	plcbit Update;
	plcbit SetIntegrationPart;
	plcbit HoldOut;
	plcbit EnableTracking;
	plcbit Busy;
	plcbit Active;
	plcbit Error;
	plcbit UpdateDone;
	plcbit TrackingActive;
};
_BUR_PUBLIC void MTBasicsPID(struct MTBasicsPID* inst);
_BUR_LOCAL struct MTBasicsPID PIDFubs;
_BUR_LOCAL MTPIDParametersType PIDParams;
