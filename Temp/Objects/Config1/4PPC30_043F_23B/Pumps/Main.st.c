#define _DEFAULT_INCLUDE
#include <bur\plctypes.h>
#include "C:/Projects/As_Wc/Temp/Objects/Config1/4PPC30_043F_23B/Pumps/Mainst.h"
#line 1 "C:/Projects/As_Wc/Logical/Source/Code/Pump/Pumps/Main.nodebug"
#line 1 "C:/Projects/As_Wc/Logical/Source/Code/Pump/Pumps/Main.st"
void __BUR__ENTRY_INIT_FUNCT__(void){{

(PIDFubs.Enable=1);
(PIDFubs.MaxOut=16384);
(PIDFubs.MinOut=0);

(PIDParams.Gain=20);
(PIDParams.IntegrationTime=1.00000001490116119385E-01);
(PIDParams.DerivativeTime=0);
(PIDParams.FilterTime=0);

(PIDFubs.PIDParameters=*(struct MTPIDParametersType*)&PIDParams);

}}
#line 14 "C:/Projects/As_Wc/Logical/Source/Code/Pump/Pumps/Main.nodebug"
#line 16 "C:/Projects/As_Wc/Logical/Source/Code/Pump/Pumps/Main.st"
void _CYCLIC __BUR__ENTRY_CYCLIC_FUNCT__(void){{


if((gExtDev.WaterPump.start^1)){
(gExtDev.WaterPump.analogOutput=0);
(PIDFubs.Enable=0);

}else{

if((((unsigned long)(unsigned char)gExtDev.WaterPump.mode==(unsigned long)(unsigned char)1))){
(gExtDev.WaterPump.analogOutput=(signed short)((gExtDev.WaterPump.setPerc*1.63839996337890625000E+02)>=0.0?(gExtDev.WaterPump.setPerc*1.63839996337890625000E+02)+0.5:(gExtDev.WaterPump.setPerc*1.63839996337890625000E+02)-0.5));


}else if((((unsigned long)(unsigned char)gExtDev.WaterPump.mode==(unsigned long)(unsigned char)2))){


(PIDFubs.ActValue=gExtDev.Flowmeter.flow);
(PIDFubs.SetValue=gExtDev.WaterPump.setFlow);
(PIDFubs.Enable=1);

(gExtDev.WaterPump.analogOutput=(signed short)(PIDFubs.Out>=0.0?PIDFubs.Out+0.5:PIDFubs.Out-0.5));


}else{
(PIDFubs.Enable=0);
(gExtDev.WaterPump.analogOutput=0);
(gExtDev.WaterPump.start=0);
}

}


if(PIDFubs.UpdateDone){
(PIDFubs.Update=0);
}

(gExtDev.WaterPump.voltage=((gExtDev.WaterPump.analogOutput/1.63839996337890625000E+02)*2.39999994635581970215E-01));

MTBasicsPID(&PIDFubs);

}}
#line 56 "C:/Projects/As_Wc/Logical/Source/Code/Pump/Pumps/Main.nodebug"

void __AS__ImplInitMain_st(void){__BUR__ENTRY_INIT_FUNCT__();}

__asm__(".section \".plc\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/Detection/gDetUsbTyp.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/StringCreator/gStringSupportTyp.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/FileSupport/gFileSupportTyp.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsUSB/AsUSB.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/asstring/asstring.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTBasics/MTBasics.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTTypes/MTTypes.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTFilter/MTFilter.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsUSB/AsUSB.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/asstring/asstring.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTBasics/MTBasics.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTTypes/MTTypes.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTFilter/MTFilter.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/Detection/gDetUsbVar.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/StringCreator/gStringSupportVar.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/FileSupport/gFileSupportVar.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsUSB/AsUSB.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/asstring/asstring.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTBasics/MTBasics.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTTypes/MTTypes.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTFilter/MTFilter.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Pump/Pumps/Types.typ\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Pump/Pumps/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"C:/Projects/As_Wc/Temp/Objects/Config1/4PPC30_043F_23B/Pumps/Main.st.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"plcreplace \\\"C:/Projects/As_Wc/Temp/Objects/Config1/4PPC30_043F_23B/Pumps/Main.st.c\\\" \\\"C:/Projects/As_Wc/Logical/Source/Code/Pump/Pumps/Main.st\\\"\\n\"");
__asm__(".previous");
