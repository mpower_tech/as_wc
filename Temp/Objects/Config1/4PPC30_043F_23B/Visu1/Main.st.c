#define _DEFAULT_INCLUDE
#include <bur\plctypes.h>
#include "C:/Projects/As_Wc/Temp/Objects/Config1/4PPC30_043F_23B/Visu1/Mainst.h"
#line 1 "C:/Projects/As_Wc/Logical/Source/Code/Visu/Visu/Main.nodebug"
#line 2 "C:/Projects/As_Wc/Logical/Source/Code/Visu/Visu/Main.st"
void __BUR__ENTRY_INIT_FUNCT__(void){{

(lVisu.Layers.Heater=1);
(lVisu.Layers.Pumps=0);

(lVisu.Scale.flowScale=1.00000000000000000000E+01);
(lVisu.Scale.frequencyScale=1.80000000000000000000E+01);
(lVisu.Scale.thermalPowerScale=3.00000000000000000000E+02);
(lVisu.Scale.tempScale=1.00000000000000000000E+02);
(lVisu.Scale.tempMinScale=0.00000000000000000000E+00);
}}
#line 12 "C:/Projects/As_Wc/Logical/Source/Code/Visu/Visu/Main.nodebug"
#line 14 "C:/Projects/As_Wc/Logical/Source/Code/Visu/Visu/Main.st"
void _CYCLIC __BUR__ENTRY_CYCLIC_FUNCT__(void){{


if((((unsigned long)(unsigned char)gExtDev.WaterPump.mode==(unsigned long)(unsigned char)0))){
(((_2byte_bit_field_*)(&lVisu.Button.PumpTurnOn))->bit1=1);
(lVisu.Blokade.Manual=2);
(lVisu.Blokade.Auto=2);

}else if((((unsigned long)(unsigned char)gExtDev.WaterPump.mode==(unsigned long)(unsigned char)1))){
(((_2byte_bit_field_*)(&lVisu.Button.PumpTurnOn))->bit1=0);
(lVisu.Blokade.Manual=0);
(lVisu.Blokade.Auto=2);

}else if((((unsigned long)(unsigned char)gExtDev.WaterPump.mode==(unsigned long)(unsigned char)2))){
(((_2byte_bit_field_*)(&lVisu.Button.PumpTurnOn))->bit1=0);
(lVisu.Blokade.Manual=2);
(lVisu.Blokade.Auto=0);
}


if(gExtDev.TempControl.start){
(((_2byte_bit_field_*)(&lVisu.Button.CoolerMachine))->bit1=1);
(((_2byte_bit_field_*)(&lVisu.Button.Heater))->bit1=1);
}else{
(((_2byte_bit_field_*)(&lVisu.Button.CoolerMachine))->bit1=0);
(((_2byte_bit_field_*)(&lVisu.Button.Heater))->bit1=0);
}


if((((gExtDev.TempControl.start^1)&gExtDev.Heater)|gExtDev.WaterPumpCoolerAndFan)){
(((_2byte_bit_field_*)(&lVisu.Button.ConstValueStart))->bit1=1);
}else{
(((_2byte_bit_field_*)(&lVisu.Button.ConstValueStart))->bit1=0);
}


}imp1_end2_0:;}
#line 50 "C:/Projects/As_Wc/Logical/Source/Code/Visu/Visu/Main.nodebug"

void __AS__ImplInitMain_st(void){__BUR__ENTRY_INIT_FUNCT__();}

__asm__(".section \".plc\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/Detection/gDetUsbTyp.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/StringCreator/gStringSupportTyp.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/FileSupport/gFileSupportTyp.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsUSB/AsUSB.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/asstring/asstring.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTBasics/MTBasics.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTTypes/MTTypes.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTFilter/MTFilter.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsUSB/AsUSB.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/asstring/asstring.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTBasics/MTBasics.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTTypes/MTTypes.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTFilter/MTFilter.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/Detection/gDetUsbVar.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/StringCreator/gStringSupportVar.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/FileSupport/gFileSupportVar.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsUSB/AsUSB.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/asstring/asstring.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTBasics/MTBasics.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTTypes/MTTypes.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTFilter/MTFilter.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Visu/Visu/Types.typ\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Visu/Visu/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"C:/Projects/As_Wc/Temp/Objects/Config1/4PPC30_043F_23B/Visu1/Main.st.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"plcreplace \\\"C:/Projects/As_Wc/Temp/Objects/Config1/4PPC30_043F_23B/Visu1/Main.st.c\\\" \\\"C:/Projects/As_Wc/Logical/Source/Code/Visu/Visu/Main.st\\\"\\n\"");
__asm__(".previous");
