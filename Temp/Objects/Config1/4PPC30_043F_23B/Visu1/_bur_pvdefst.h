#ifndef __AS__TYPE_VisuLayersTyp
#define __AS__TYPE_VisuLayersTyp
typedef struct VisuLayersTyp
{	signed short Pumps;
	signed short Heater;
} VisuLayersTyp;
#endif

#ifndef __AS__TYPE_VisuBtnTyp
#define __AS__TYPE_VisuBtnTyp
typedef struct VisuBtnTyp
{	signed short PumpTurnOn;
	signed short CoolerMachine;
	signed short Heater;
	signed short ConstValueStart;
} VisuBtnTyp;
#endif

#ifndef __AS__TYPE_VisuScaleTyp
#define __AS__TYPE_VisuScaleTyp
typedef struct VisuScaleTyp
{	float thermalPowerScale;
	float flowScale;
	float frequencyScale;
	float tempScale;
	float tempMinScale;
} VisuScaleTyp;
#endif

#ifndef __AS__TYPE_VisuNumericTyp
#define __AS__TYPE_VisuNumericTyp
typedef struct VisuNumericTyp
{	signed short PercVoltage;
	signed short FlowSet;
} VisuNumericTyp;
#endif

#ifndef __AS__TYPE_VisuBlokadeTyp
#define __AS__TYPE_VisuBlokadeTyp
typedef struct VisuBlokadeTyp
{	signed short Auto;
	signed short Manual;
} VisuBlokadeTyp;
#endif

#ifndef __AS__TYPE_VisuTyp
#define __AS__TYPE_VisuTyp
typedef struct VisuTyp
{	unsigned char Dialog;
	VisuLayersTyp Layers;
	VisuBtnTyp Button;
	VisuScaleTyp Scale;
	VisuNumericTyp Numeric;
	VisuBlokadeTyp Blokade;
} VisuTyp;
#endif

_BUR_LOCAL VisuTyp lVisu;
