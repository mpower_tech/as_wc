#ifndef __AS__TYPE_
#define __AS__TYPE_
static unsigned long __AS__STRCAT(unsigned long pDest, unsigned long pSrc);
typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

#ifndef __AS__TYPE_DTStructure
#define __AS__TYPE_DTStructure
typedef struct DTStructure
{	unsigned short year;
	unsigned char month;
	unsigned char day;
	unsigned char wday;
	unsigned char hour;
	unsigned char minute;
	unsigned char second;
	unsigned short millisec;
	unsigned short microsec;
} DTStructure;
#endif

#ifndef __AS__TYPE_StringSupportInterDataStringTyp
#define __AS__TYPE_StringSupportInterDataStringTyp
typedef struct StringSupportInterDataStringTyp
{	plcstring sec[3];
	plcstring minute[3];
	plcstring hour[3];
	plcstring day[3];
	plcstring year[5];
	plcstring month[3];
} StringSupportInterDataStringTyp;
#endif

struct DTStructureGetTime
{	unsigned long pDTStructure;
	unsigned short status;
	plcbit enable;
};
_BUR_PUBLIC void DTStructureGetTime(struct DTStructureGetTime* inst);
#ifndef __AS__TYPE_StringSupportInterDataTyp
#define __AS__TYPE_StringSupportInterDataTyp
typedef struct StringSupportInterDataTyp
{	DTStructure DTStruct;
	StringSupportInterDataStringTyp StringData;
	struct DTStructureGetTime GetData;
} StringSupportInterDataTyp;
#endif

#ifndef __AS__TYPE_StringSupportInterType
#define __AS__TYPE_StringSupportInterType
typedef struct StringSupportInterType
{	StringSupportInterDataTyp DataATime;
	plcstring DataToSave[201];
	unsigned short LenStringToSave;
	unsigned short LenDataToSave;
	plcstring ConvSample[101];
} StringSupportInterType;
#endif

#ifndef __AS__TYPE_StringSupportInpCmdTyp
#define __AS__TYPE_StringSupportInpCmdTyp
typedef struct StringSupportInpCmdTyp
{	plcbit NewValue;
} StringSupportInpCmdTyp;
#endif

#ifndef __AS__TYPE_StringSupportInpParTyp
#define __AS__TYPE_StringSupportInpParTyp
typedef struct StringSupportInpParTyp
{	unsigned short MaxBuforLen;
	float Samples[41];
	unsigned char NumOfSamples;
} StringSupportInpParTyp;
#endif

#ifndef __AS__TYPE_StringSupportInpTyp
#define __AS__TYPE_StringSupportInpTyp
typedef struct StringSupportInpTyp
{	StringSupportInpCmdTyp Cmd;
	StringSupportInpParTyp Params;
} StringSupportInpTyp;
#endif

#ifndef __AS__TYPE_SM_STRING_SUPPORT
#define __AS__TYPE_SM_STRING_SUPPORT
typedef enum SM_STRING_SUPPORT
{	SM_STRING_WAIT_FOR_ENABLE = 0,
	SM_STRING_PREPARE_SAMPLES = 1,
} SM_STRING_SUPPORT;
#endif

#ifndef __AS__TYPE_StringSupportOutStatesTyp
#define __AS__TYPE_StringSupportOutStatesTyp
typedef struct StringSupportOutStatesTyp
{	SM_STRING_SUPPORT SM_STRING_SUPPORT;
} StringSupportOutStatesTyp;
#endif

#ifndef __AS__TYPE_StringSupportOutStatusTyp
#define __AS__TYPE_StringSupportOutStatusTyp
typedef struct StringSupportOutStatusTyp
{	plcbit ReadyToSave;
	plcstring StringToSave[10001];
	plcbit Error;
	unsigned short FillVector;
} StringSupportOutStatusTyp;
#endif

#ifndef __AS__TYPE_StringSupportOutTyp
#define __AS__TYPE_StringSupportOutTyp
typedef struct StringSupportOutTyp
{	StringSupportOutStatesTyp States;
	StringSupportOutStatusTyp Status;
} StringSupportOutTyp;
#endif

#ifndef __AS__TYPE_StringSupportType
#define __AS__TYPE_StringSupportType
typedef struct StringSupportType
{	StringSupportInpTyp Input;
	StringSupportOutTyp Output;
} StringSupportType;
#endif

_BUR_PUBLIC plcstring* usint2str(unsigned char IN, plcstring pStr[81], unsigned long len);
_BUR_PUBLIC plcstring* uint2str(unsigned short IN, plcstring pStr[81], unsigned long len);
_BUR_PUBLIC unsigned short ftoa(float value, unsigned long pString);
_BUR_PUBLIC unsigned long strcat(unsigned long pDest, unsigned long pSrc);
_BUR_PUBLIC unsigned short strlen(unsigned long pString);
_BUR_LOCAL StringSupportInterType lStringSupport;
_GLOBAL unsigned short i;
_GLOBAL StringSupportType gStringSupport;
