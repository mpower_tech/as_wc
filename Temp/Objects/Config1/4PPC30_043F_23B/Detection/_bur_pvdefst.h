#ifndef __AS__TYPE_usbNode_typ
#define __AS__TYPE_usbNode_typ
typedef struct usbNode_typ
{	unsigned short interfaceClass;
	unsigned short interfaceSubClass;
	unsigned short interfaceProtocol;
	unsigned short vendorId;
	unsigned short productId;
	unsigned short bcdDevice;
	plcstring ifName[128];
} usbNode_typ;
#endif

#ifndef __AS__TYPE_DetUsbInterDataInterTyp
#define __AS__TYPE_DetUsbInterDataInterTyp
typedef struct DetUsbInterDataInterTyp
{	plcstring pParam[81];
	unsigned long NodeIfBuffer[10];
	usbNode_typ UsbData;
} DetUsbInterDataInterTyp;
#endif

struct UsbNodeGet
{	unsigned long nodeId;
	unsigned long pBuffer;
	unsigned long bufferSize;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void UsbNodeGet(struct UsbNodeGet* inst);
struct DevUnlink
{	unsigned long handle;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void DevUnlink(struct DevUnlink* inst);
struct DevLink
{	unsigned long pDevice;
	unsigned long pParam;
	unsigned short status;
	unsigned long handle;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void DevLink(struct DevLink* inst);
struct UsbMsDeviceReady
{	unsigned long pIfName;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
	plcbit ready;
};
_BUR_PUBLIC void UsbMsDeviceReady(struct UsbMsDeviceReady* inst);
struct UsbNodeListGet
{	unsigned long pBuffer;
	unsigned long bufferSize;
	unsigned short filterInterfaceClass;
	unsigned short filterInterfaceSubClass;
	unsigned short status;
	unsigned long allNodes;
	unsigned long listNodes;
	unsigned long attachDetachCount;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void UsbNodeListGet(struct UsbNodeListGet* inst);
struct TON
{	plctime PT;
	plctime ET;
	plctime StartTime;
	unsigned long Restart;
	plcbit IN;
	plcbit Q;
	plcbit M;
};
_BUR_PUBLIC void TON(struct TON* inst);
#ifndef __AS__TYPE_DetUsbInternalType
#define __AS__TYPE_DetUsbInternalType
typedef struct DetUsbInternalType
{	DetUsbInterDataInterTyp DataInternal;
	struct UsbNodeGet UsbNodeGet;
	struct DevUnlink DevUnlink;
	struct DevLink DevLink;
	struct UsbMsDeviceReady UsbMsDeviceReady;
	struct UsbNodeListGet UsbNodeListGet;
	struct TON Redetection;
} DetUsbInternalType;
#endif

_BUR_LOCAL DetUsbInternalType lDetUsbInternal;
