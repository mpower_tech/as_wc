#ifndef __AS__TYPE_
#define __AS__TYPE_
static unsigned long __AS__MEMSET(unsigned long pDest, unsigned char value, unsigned long length);
static unsigned long __AS__STRCAT(unsigned long pDest, unsigned long pSrc);
typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

#ifndef __AS__TYPE_usbNode_typ
#define __AS__TYPE_usbNode_typ
typedef struct usbNode_typ
{	unsigned short interfaceClass;
	unsigned short interfaceSubClass;
	unsigned short interfaceProtocol;
	unsigned short vendorId;
	unsigned short productId;
	unsigned short bcdDevice;
	plcstring ifName[128];
} usbNode_typ;
#endif

#ifndef __AS__TYPE_DetUsbInterDataInterTyp
#define __AS__TYPE_DetUsbInterDataInterTyp
typedef struct DetUsbInterDataInterTyp
{	plcstring pParam[81];
	unsigned long NodeIfBuffer[10];
	usbNode_typ UsbData;
} DetUsbInterDataInterTyp;
#endif

struct UsbNodeGet
{	unsigned long nodeId;
	unsigned long pBuffer;
	unsigned long bufferSize;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void UsbNodeGet(struct UsbNodeGet* inst);
struct DevUnlink
{	unsigned long handle;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void DevUnlink(struct DevUnlink* inst);
struct DevLink
{	unsigned long pDevice;
	unsigned long pParam;
	unsigned short status;
	unsigned long handle;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void DevLink(struct DevLink* inst);
struct UsbMsDeviceReady
{	unsigned long pIfName;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
	plcbit ready;
};
_BUR_PUBLIC void UsbMsDeviceReady(struct UsbMsDeviceReady* inst);
struct UsbNodeListGet
{	unsigned long pBuffer;
	unsigned long bufferSize;
	unsigned short filterInterfaceClass;
	unsigned short filterInterfaceSubClass;
	unsigned short status;
	unsigned long allNodes;
	unsigned long listNodes;
	unsigned long attachDetachCount;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void UsbNodeListGet(struct UsbNodeListGet* inst);
struct TON
{	plctime PT;
	plctime ET;
	plctime StartTime;
	unsigned long Restart;
	plcbit IN;
	plcbit Q;
	plcbit M;
};
_BUR_PUBLIC void TON(struct TON* inst);
#ifndef __AS__TYPE_DetUsbInternalType
#define __AS__TYPE_DetUsbInternalType
typedef struct DetUsbInternalType
{	DetUsbInterDataInterTyp DataInternal;
	struct UsbNodeGet UsbNodeGet;
	struct DevUnlink DevUnlink;
	struct DevLink DevLink;
	struct UsbMsDeviceReady UsbMsDeviceReady;
	struct UsbNodeListGet UsbNodeListGet;
	struct TON Redetection;
} DetUsbInternalType;
#endif

#ifndef __AS__TYPE_DetUsbInpCmdTyp
#define __AS__TYPE_DetUsbInpCmdTyp
typedef struct DetUsbInpCmdTyp
{	plcbit Enable;
	plcbit Reset;
	plcbit Stop;
	plcbit ForceQuickDet;
} DetUsbInpCmdTyp;
#endif

#ifndef __AS__TYPE_DetUsbInpParTyp
#define __AS__TYPE_DetUsbInpParTyp
typedef struct DetUsbInpParTyp
{	plctime AutoDetectionTime;
} DetUsbInpParTyp;
#endif

#ifndef __AS__TYPE_DetUsbInpTyp
#define __AS__TYPE_DetUsbInpTyp
typedef struct DetUsbInpTyp
{	DetUsbInpCmdTyp Cmd;
	DetUsbInpParTyp Params;
} DetUsbInpTyp;
#endif

#ifndef __AS__TYPE_USB_DET_SM
#define __AS__TYPE_USB_DET_SM
typedef enum USB_DET_SM
{	USB_DET_INIT = 0,
	USB_DET_GET_NODE_LIST = 1,
	USB_DET_GET_NODE_INFO = 2,
	USB_DET_DELETE_DEV_LINK = 3,
	USB_DET_QUICK_DETECTION = 4,
	USB_DET_CREATE_DEV_LINK = 5,
	USB_DET_WAIT = 6,
	USB_DET_ERROR = 7,
} USB_DET_SM;
#endif

#ifndef __AS__TYPE_DetUsbOutStatesTyp
#define __AS__TYPE_DetUsbOutStatesTyp
typedef struct DetUsbOutStatesTyp
{	USB_DET_SM SM_DET_USB;
} DetUsbOutStatesTyp;
#endif

#ifndef __AS__TYPE_DetUsbOutStatusInterTyp
#define __AS__TYPE_DetUsbOutStatusInterTyp
typedef struct DetUsbOutStatusInterTyp
{	unsigned char Index;
	unsigned char Time;
	signed short NumOfError;
} DetUsbOutStatusInterTyp;
#endif

#ifndef __AS__TYPE_DetUsbOutStatusTyp
#define __AS__TYPE_DetUsbOutStatusTyp
typedef struct DetUsbOutStatusTyp
{	unsigned long DevHandle;
	plcstring DevLink[10][51];
	plcbit DiscDetected;
	plcbit Error;
	unsigned char NumOfDev;
	DetUsbOutStatusInterTyp Internal;
} DetUsbOutStatusTyp;
#endif

#ifndef __AS__TYPE_DetUsbOutTyp
#define __AS__TYPE_DetUsbOutTyp
typedef struct DetUsbOutTyp
{	DetUsbOutStatesTyp States;
	DetUsbOutStatusTyp Status;
} DetUsbOutTyp;
#endif

#ifndef __AS__TYPE_DetUsbType
#define __AS__TYPE_DetUsbType
typedef struct DetUsbType
{	DetUsbInpTyp Input;
	DetUsbOutTyp Output;
} DetUsbType;
#endif

_BUR_PUBLIC unsigned long memset(unsigned long pDest, unsigned char value, unsigned long length);
_BUR_PUBLIC unsigned long strcat(unsigned long pDest, unsigned long pSrc);
_BUR_LOCAL DetUsbInternalType lDetUsbInternal;
_GLOBAL DetUsbType gDetUsb;
_GLOBAL unsigned short ERR_OK;
_GLOBAL unsigned short ERR_FUB_BUSY;
_GLOBAL unsigned short asusbERR_USB_NOTFOUND;
