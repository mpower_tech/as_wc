#define _DEFAULT_INCLUDE
#include <bur\plctypes.h>
#include "C:/Projects/As_Wc/Temp/Objects/Config1/4PPC30_043F_23B/Sensors/Mainst.h"
#line 1 "C:/Projects/As_Wc/Logical/Source/Code/Sensors/Sensors/Main.nodebug"
#line 1 "C:/Projects/As_Wc/Logical/Source/Code/Sensors/Sensors/Main.st"
void __BUR__ENTRY_INIT_FUNCT__(void){{

(MovingAverageFiowMeter.WindowLength=6);
(MovingAverageFiowMeter.Enable=1);

}}
#line 6 "C:/Projects/As_Wc/Logical/Source/Code/Sensors/Sensors/Main.nodebug"
#line 8 "C:/Projects/As_Wc/Logical/Source/Code/Sensors/Sensors/Main.st"
void _CYCLIC __BUR__ENTRY_CYCLIC_FUNCT__(void){{



__AS__Action__Temperature_measurement();



__AS__Action__Flow_measurement();



__AS__Action__Thermal_measurement();



__AS__Action__UsbAndModbus();

}}
#line 26 "C:/Projects/As_Wc/Logical/Source/Code/Sensors/Sensors/Main.nodebug"
#line 2 "C:/Projects/As_Wc/Logical/Source/Code/Sensors/Sensors/TemperatureAction.st"
static void __AS__Action__Temperature_measurement(void){
{


if(((((unsigned long)(unsigned char)((_1byte_bit_field_*)(&gExtDev.TempMeter.Inlet.mainState))->bit0==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)((_1byte_bit_field_*)(&gExtDev.TempMeter.Inlet.mainState))->bit1==(unsigned long)(unsigned char)0)))){
(gExtDev.TempMeter.Inlet.state=1);
}else{
(gExtDev.TempMeter.Inlet.state=0);
}


if(((((unsigned long)(unsigned char)((_1byte_bit_field_*)(&gExtDev.TempMeter.Inlet.mainState))->bit2==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)((_1byte_bit_field_*)(&gExtDev.TempMeter.Inlet.mainState))->bit3==(unsigned long)(unsigned char)0)))){
(gExtDev.TempMeter.Outlet.state=1);
}else{
(gExtDev.TempMeter.Outlet.state=0);
}


if(((((unsigned long)(unsigned char)((_1byte_bit_field_*)(&gExtDev.TempMeter.Inlet.mainState))->bit4==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)((_1byte_bit_field_*)(&gExtDev.TempMeter.Inlet.mainState))->bit5==(unsigned long)(unsigned char)0)))){
(gExtDev.TempMeter.Tank.state=1);
}else{
(gExtDev.TempMeter.Tank.state=0);
}


if(gExtDev.TempMeter.Inlet.state){
(gExtDev.TempMeter.Inlet.temp=((float)gExtDev.TempMeter.Inlet.temp0x/1.00000000000000000000E+02));
}else{
(gExtDev.TempMeter.Inlet.temp=0.00000000000000000000E+00);
}

if(gExtDev.TempMeter.Outlet.state){
(gExtDev.TempMeter.Outlet.temp=((float)gExtDev.TempMeter.Outlet.temp0x/1.00000000000000000000E+02));
}else{
(gExtDev.TempMeter.Outlet.temp=0.00000000000000000000E+00);
}

if(gExtDev.TempMeter.Tank.state){
(gExtDev.TempMeter.Tank.temp=((float)gExtDev.TempMeter.Tank.temp0x/1.00000000000000000000E+02));
}else{
(gExtDev.TempMeter.Tank.temp=0.00000000000000000000E+00);
}

}imp2_end5_0:;}
#line 29 "C:/Projects/As_Wc/Logical/Source/Code/Sensors/Sensors/Main.nodebug"
#line 2 "C:/Projects/As_Wc/Logical/Source/Code/Sensors/Sensors/FlowAction.st"
static void __AS__Action__Flow_measurement(void){
{

if(((((unsigned long)(unsigned short)gExtDev.Flowmeter.pulses==(unsigned long)(unsigned short)65535))|(((unsigned long)(unsigned short)gExtDev.Flowmeter.pulses==(unsigned long)(unsigned short)0)))){
(gExtDev.Flowmeter.frequency=0);
}else{
(gExtDev.Flowmeter.frequency=((1/(gExtDev.Flowmeter.pulses/1.87500000000000000000E+05))/2.00000000000000000000E+00));
}


(gExtDev.Flowmeter.flow=(gExtDev.Flowmeter.frequency*(6.00000000000000000000E+01/294)));

(MovingAverageFiowMeter.In=gExtDev.Flowmeter.flow);
MTFilterMovingAverage(&MovingAverageFiowMeter);

(gExtDev.Flowmeter.flow=MovingAverageFiowMeter.Out);

}}
#line 29 "C:/Projects/As_Wc/Logical/Source/Code/Sensors/Sensors/Main.nodebug"
#line 2 "C:/Projects/As_Wc/Logical/Source/Code/Sensors/Sensors/ThermalPwrAction.st"
static void __AS__Action__Thermal_measurement(void){
{





(gExtDev.PowerThermal.powerTerm=(((gExtDev.Flowmeter.flow/6.00000000000000000000E+01)*4.18989990234375000000E+03)*(gExtDev.TempMeter.Inlet.temp-gExtDev.TempMeter.Outlet.temp)));

}}
#line 29 "C:/Projects/As_Wc/Logical/Source/Code/Sensors/Sensors/Main.nodebug"
#line 2 "C:/Projects/As_Wc/Logical/Source/Code/Sensors/Sensors/UsbAndMdbAction.st"
static void __AS__Action__UsbAndModbus(void){
{

(gStringSupport.Input.Params.NumOfSamples=6);
(gStringSupport.Input.Params.Samples[0]=gExtDev.TempMeter.Inlet.temp);
(gStringSupport.Input.Params.Samples[1]=gExtDev.TempMeter.Outlet.temp);
(gStringSupport.Input.Params.Samples[2]=gExtDev.TempMeter.Tank.temp);
(gStringSupport.Input.Params.Samples[3]=gExtDev.Flowmeter.flow);
(gStringSupport.Input.Params.Samples[4]=gExtDev.PowerThermal.powerTerm);
(gStringSupport.Input.Params.Samples[5]=gExtDev.WaterPump.setPerc);
(gStringSupport.Input.Cmd.NewValue=1);


(gExtDev.ModbusOut.actFlow=(unsigned short)((gExtDev.Flowmeter.flow*100)>=0.0?(gExtDev.Flowmeter.flow*100)+0.5:(gExtDev.Flowmeter.flow*100)-0.5));
(gExtDev.ModbusOut.powerThermal=(unsigned short)((gExtDev.PowerThermal.powerTerm*100)>=0.0?(gExtDev.PowerThermal.powerTerm*100)+0.5:(gExtDev.PowerThermal.powerTerm*100)-0.5));
(gExtDev.ModbusOut.tempInlet=(unsigned short)gExtDev.TempMeter.Inlet.temp0x);
(gExtDev.ModbusOut.tempOutlet=(unsigned short)gExtDev.TempMeter.Outlet.temp0x);
(gExtDev.ModbusOut.tempTank=(unsigned short)gExtDev.TempMeter.Tank.temp0x);


}}
#line 29 "C:/Projects/As_Wc/Logical/Source/Code/Sensors/Sensors/Main.nodebug"

void __AS__ImplInitMain_st(void){__BUR__ENTRY_INIT_FUNCT__();}

__asm__(".section \".plc\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/Detection/gDetUsbTyp.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/StringCreator/gStringSupportTyp.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/FileSupport/gFileSupportTyp.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsUSB/AsUSB.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/asstring/asstring.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTBasics/MTBasics.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTTypes/MTTypes.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTFilter/MTFilter.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsUSB/AsUSB.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/asstring/asstring.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTBasics/MTBasics.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTTypes/MTTypes.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTFilter/MTFilter.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/Detection/gDetUsbVar.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/StringCreator/gStringSupportVar.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/FileSupport/gFileSupportVar.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsUSB/AsUSB.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/asstring/asstring.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTBasics/MTBasics.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTTypes/MTTypes.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTFilter/MTFilter.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Sensors/Sensors/Types.typ\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Sensors/Sensors/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"C:/Projects/As_Wc/Temp/Objects/Config1/4PPC30_043F_23B/Sensors/Main.st.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"plcreplace \\\"C:/Projects/As_Wc/Temp/Objects/Config1/4PPC30_043F_23B/Sensors/Main.st.c\\\" \\\"C:/Projects/As_Wc/Logical/Source/Code/Sensors/Sensors/Main.st\\\"\\n\"");
__asm__(".previous");
