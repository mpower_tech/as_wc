#ifndef __AS__TYPE_
#define __AS__TYPE_
typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

#ifndef __AS__TYPE_ExtDevTempTyp
#define __AS__TYPE_ExtDevTempTyp
typedef struct ExtDevTempTyp
{	plcbit state;
	float temp;
	signed long temp0x;
	unsigned char mainState;
} ExtDevTempTyp;
#endif

#ifndef __AS__TYPE_ExtTempmeterTyp
#define __AS__TYPE_ExtTempmeterTyp
typedef struct ExtTempmeterTyp
{	ExtDevTempTyp Inlet;
	ExtDevTempTyp Outlet;
	ExtDevTempTyp Tank;
} ExtTempmeterTyp;
#endif

#ifndef __AS__TYPE_ExtDevFlowmeterTyp
#define __AS__TYPE_ExtDevFlowmeterTyp
typedef struct ExtDevFlowmeterTyp
{	unsigned short pulses;
	float flow;
	float frequency;
} ExtDevFlowmeterTyp;
#endif

#ifndef __AS__TYPE_ExtDevWaterpumpTyp
#define __AS__TYPE_ExtDevWaterpumpTyp
typedef struct ExtDevWaterpumpTyp
{	unsigned char mode;
	plcbit start;
	unsigned char setPerc;
	float setFlow;
	signed short analogOutput;
	float voltage;
} ExtDevWaterpumpTyp;
#endif

#ifndef __AS__TYPE_ExtDevModulesTyp
#define __AS__TYPE_ExtDevModulesTyp
typedef struct ExtDevModulesTyp
{	plcbit ATB312;
	plcbit CM8281;
	plcbit AO4622;
} ExtDevModulesTyp;
#endif

#ifndef __AS__TYPE_ExtDevModbusOutTyp
#define __AS__TYPE_ExtDevModbusOutTyp
typedef struct ExtDevModbusOutTyp
{	unsigned short actFlow;
	unsigned short powerThermal;
	unsigned short tempInlet;
	unsigned short tempOutlet;
	unsigned short tempTank;
} ExtDevModbusOutTyp;
#endif

#ifndef __AS__TYPE_ExtPowerThermalTyp
#define __AS__TYPE_ExtPowerThermalTyp
typedef struct ExtPowerThermalTyp
{	float powerTerm;
} ExtPowerThermalTyp;
#endif

#ifndef __AS__TYPE_ExtDevTempControlTyp
#define __AS__TYPE_ExtDevTempControlTyp
typedef struct ExtDevTempControlTyp
{	float setTankTemp;
	plcbit start;
	float histeresis;
	unsigned char SM;
} ExtDevTempControlTyp;
#endif

#ifndef __AS__TYPE_ExtDevType
#define __AS__TYPE_ExtDevType
typedef struct ExtDevType
{	ExtTempmeterTyp TempMeter;
	ExtDevFlowmeterTyp Flowmeter;
	ExtDevWaterpumpTyp WaterPump;
	plcbit WaterPumpCoolerAndFan;
	plcbit Heater;
	ExtDevModulesTyp Modules;
	ExtDevModbusOutTyp ModbusOut;
	ExtPowerThermalTyp PowerThermal;
	ExtDevTempControlTyp TempControl;
} ExtDevType;
#endif

#ifndef __AS__TYPE_StringSupportInpCmdTyp
#define __AS__TYPE_StringSupportInpCmdTyp
typedef struct StringSupportInpCmdTyp
{	plcbit NewValue;
} StringSupportInpCmdTyp;
#endif

#ifndef __AS__TYPE_StringSupportInpParTyp
#define __AS__TYPE_StringSupportInpParTyp
typedef struct StringSupportInpParTyp
{	unsigned short MaxBuforLen;
	float Samples[41];
	unsigned char NumOfSamples;
} StringSupportInpParTyp;
#endif

#ifndef __AS__TYPE_StringSupportInpTyp
#define __AS__TYPE_StringSupportInpTyp
typedef struct StringSupportInpTyp
{	StringSupportInpCmdTyp Cmd;
	StringSupportInpParTyp Params;
} StringSupportInpTyp;
#endif

#ifndef __AS__TYPE_SM_STRING_SUPPORT
#define __AS__TYPE_SM_STRING_SUPPORT
typedef enum SM_STRING_SUPPORT
{	SM_STRING_WAIT_FOR_ENABLE = 0,
	SM_STRING_PREPARE_SAMPLES = 1,
} SM_STRING_SUPPORT;
#endif

#ifndef __AS__TYPE_StringSupportOutStatesTyp
#define __AS__TYPE_StringSupportOutStatesTyp
typedef struct StringSupportOutStatesTyp
{	SM_STRING_SUPPORT SM_STRING_SUPPORT;
} StringSupportOutStatesTyp;
#endif

#ifndef __AS__TYPE_StringSupportOutStatusTyp
#define __AS__TYPE_StringSupportOutStatusTyp
typedef struct StringSupportOutStatusTyp
{	plcbit ReadyToSave;
	plcstring StringToSave[10001];
	plcbit Error;
	unsigned short FillVector;
} StringSupportOutStatusTyp;
#endif

#ifndef __AS__TYPE_StringSupportOutTyp
#define __AS__TYPE_StringSupportOutTyp
typedef struct StringSupportOutTyp
{	StringSupportOutStatesTyp States;
	StringSupportOutStatusTyp Status;
} StringSupportOutTyp;
#endif

#ifndef __AS__TYPE_StringSupportType
#define __AS__TYPE_StringSupportType
typedef struct StringSupportType
{	StringSupportInpTyp Input;
	StringSupportOutTyp Output;
} StringSupportType;
#endif

#ifndef __AS__TYPE_MTFilterMovingAvgInternalType
#define __AS__TYPE_MTFilterMovingAvgInternalType
typedef struct MTFilterMovingAvgInternalType
{	float Ts;
	plcbit ParameterValid;
	unsigned short NumOfElmements;
	double Sum;
	unsigned long pBuffer;
	unsigned short CntAct;
	unsigned short CntOld;
	unsigned short WindowLength;
	unsigned short BufferLength;
	plcbit BufferFull;
	plcbit MemAllocated;
	plcbit Update;
	plcbit UpdateOld;
	plcbit EnableOld;
	unsigned short statusTMP;
} MTFilterMovingAvgInternalType;
#endif

struct MTFilterMovingAverage
{	unsigned short WindowLength;
	float In;
	signed long StatusID;
	float Out;
	MTFilterMovingAvgInternalType Internal;
	plcbit Enable;
	plcbit Update;
	plcbit Active;
	plcbit Error;
	plcbit UpdateDone;
};
_BUR_PUBLIC void MTFilterMovingAverage(struct MTFilterMovingAverage* inst);
_BUR_LOCAL struct MTFilterMovingAverage MovingAverageFiowMeter;
_GLOBAL ExtDevType gExtDev;
_GLOBAL StringSupportType gStringSupport;
static void __AS__Action__Temperature_measurement(void);
static void __AS__Action__Flow_measurement(void);
static void __AS__Action__Thermal_measurement(void);
static void __AS__Action__UsbAndModbus(void);
