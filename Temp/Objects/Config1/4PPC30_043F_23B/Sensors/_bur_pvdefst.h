#ifndef __AS__TYPE_MTFilterMovingAvgInternalType
#define __AS__TYPE_MTFilterMovingAvgInternalType
typedef struct MTFilterMovingAvgInternalType
{	float Ts;
	plcbit ParameterValid;
	unsigned short NumOfElmements;
	double Sum;
	unsigned long pBuffer;
	unsigned short CntAct;
	unsigned short CntOld;
	unsigned short WindowLength;
	unsigned short BufferLength;
	plcbit BufferFull;
	plcbit MemAllocated;
	plcbit Update;
	plcbit UpdateOld;
	plcbit EnableOld;
	unsigned short statusTMP;
} MTFilterMovingAvgInternalType;
#endif

struct MTFilterMovingAverage
{	unsigned short WindowLength;
	float In;
	signed long StatusID;
	float Out;
	MTFilterMovingAvgInternalType Internal;
	plcbit Enable;
	plcbit Update;
	plcbit Active;
	plcbit Error;
	plcbit UpdateDone;
};
_BUR_PUBLIC void MTFilterMovingAverage(struct MTFilterMovingAverage* inst);
_BUR_LOCAL plcbit start;
_BUR_LOCAL unsigned short step;
_BUR_LOCAL struct MTFilterMovingAverage MovingAverageFiowMeter;
