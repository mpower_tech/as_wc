struct DTStructureGetTime
{	unsigned long pDTStructure;
	unsigned short status;
	plcbit enable;
};
_BUR_PUBLIC void DTStructureGetTime(struct DTStructureGetTime* inst);
struct TON
{	plctime PT;
	plctime ET;
	plctime StartTime;
	unsigned long Restart;
	plcbit IN;
	plcbit Q;
	plcbit M;
};
_BUR_PUBLIC void TON(struct TON* inst);
#ifndef __AS__TYPE_FileSupportInterFileNameSpptTyp
#define __AS__TYPE_FileSupportInterFileNameSpptTyp
typedef struct FileSupportInterFileNameSpptTyp
{	plcstring Year[5];
	plcstring Month[3];
	plcstring InternalFileName[81];
} FileSupportInterFileNameSpptTyp;
#endif

struct FileWrite
{	unsigned long ident;
	unsigned long offset;
	unsigned long pSrc;
	unsigned long len;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileWrite(struct FileWrite* inst);
struct FileOpen
{	unsigned long pDevice;
	unsigned long pFile;
	unsigned char mode;
	unsigned short status;
	unsigned long ident;
	unsigned long filelen;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileOpen(struct FileOpen* inst);
struct FileClose
{	unsigned long ident;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileClose(struct FileClose* inst);
struct FileCreate
{	unsigned long pDevice;
	unsigned long pFile;
	unsigned short status;
	unsigned long ident;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileCreate(struct FileCreate* inst);
#ifndef __AS__TYPE_FileSupportInterFileTyp
#define __AS__TYPE_FileSupportInterFileTyp
typedef struct FileSupportInterFileTyp
{	struct FileWrite FileWrite;
	struct FileOpen FileOpen;
	struct FileClose FileClose;
	struct FileCreate FileCreate;
} FileSupportInterFileTyp;
#endif

#ifndef __AS__TYPE_DTStructure
#define __AS__TYPE_DTStructure
typedef struct DTStructure
{	unsigned short year;
	unsigned char month;
	unsigned char day;
	unsigned char wday;
	unsigned char hour;
	unsigned char minute;
	unsigned char second;
	unsigned short millisec;
	unsigned short microsec;
} DTStructure;
#endif

#ifndef __AS__TYPE_FileSupportBufforDataTyp
#define __AS__TYPE_FileSupportBufforDataTyp
typedef struct FileSupportBufforDataTyp
{	plcstring Buffor[3][10001];
	unsigned char Index;
	plcstring FileNameIndex[3][81];
} FileSupportBufforDataTyp;
#endif

#ifndef __AS__TYPE_FileSupportInterType
#define __AS__TYPE_FileSupportInterType
typedef struct FileSupportInterType
{	struct DTStructureGetTime GetData;
	struct TON TON_SaveDepend;
	struct TON TON_MaxSaveDelay;
	FileSupportInterFileNameSpptTyp FileNameSupport;
	FileSupportInterFileTyp FileSupport;
	unsigned long Offset;
	DTStructure DTStruct;
	FileSupportBufforDataTyp FileSupportBufforData;
	plcbit Init;
	plcstring Header[301];
} FileSupportInterType;
#endif

_BUR_LOCAL FileSupportInterType lFileSupport;
