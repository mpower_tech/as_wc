#ifndef __AS__TYPE_
#define __AS__TYPE_
static signed long __AS__STRING_CMP(char* pstr1, char* pstr2);
static unsigned long __AS__MEMSET(unsigned long pDest, unsigned char value, unsigned long length);
static unsigned long __AS__STRCAT(unsigned long pDest, unsigned long pSrc);
typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

struct DTStructureGetTime
{	unsigned long pDTStructure;
	unsigned short status;
	plcbit enable;
};
_BUR_PUBLIC void DTStructureGetTime(struct DTStructureGetTime* inst);
struct TON
{	plctime PT;
	plctime ET;
	plctime StartTime;
	unsigned long Restart;
	plcbit IN;
	plcbit Q;
	plcbit M;
};
_BUR_PUBLIC void TON(struct TON* inst);
#ifndef __AS__TYPE_FileSupportInterFileNameSpptTyp
#define __AS__TYPE_FileSupportInterFileNameSpptTyp
typedef struct FileSupportInterFileNameSpptTyp
{	plcstring Year[5];
	plcstring Month[3];
	plcstring InternalFileName[81];
} FileSupportInterFileNameSpptTyp;
#endif

struct FileWrite
{	unsigned long ident;
	unsigned long offset;
	unsigned long pSrc;
	unsigned long len;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileWrite(struct FileWrite* inst);
struct FileOpen
{	unsigned long pDevice;
	unsigned long pFile;
	unsigned char mode;
	unsigned short status;
	unsigned long ident;
	unsigned long filelen;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileOpen(struct FileOpen* inst);
struct FileClose
{	unsigned long ident;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileClose(struct FileClose* inst);
struct FileCreate
{	unsigned long pDevice;
	unsigned long pFile;
	unsigned short status;
	unsigned long ident;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileCreate(struct FileCreate* inst);
#ifndef __AS__TYPE_FileSupportInterFileTyp
#define __AS__TYPE_FileSupportInterFileTyp
typedef struct FileSupportInterFileTyp
{	struct FileWrite FileWrite;
	struct FileOpen FileOpen;
	struct FileClose FileClose;
	struct FileCreate FileCreate;
} FileSupportInterFileTyp;
#endif

#ifndef __AS__TYPE_DTStructure
#define __AS__TYPE_DTStructure
typedef struct DTStructure
{	unsigned short year;
	unsigned char month;
	unsigned char day;
	unsigned char wday;
	unsigned char hour;
	unsigned char minute;
	unsigned char second;
	unsigned short millisec;
	unsigned short microsec;
} DTStructure;
#endif

#ifndef __AS__TYPE_FileSupportBufforDataTyp
#define __AS__TYPE_FileSupportBufforDataTyp
typedef struct FileSupportBufforDataTyp
{	plcstring Buffor[3][10001];
	unsigned char Index;
	plcstring FileNameIndex[3][81];
} FileSupportBufforDataTyp;
#endif

#ifndef __AS__TYPE_FileSupportInterType
#define __AS__TYPE_FileSupportInterType
typedef struct FileSupportInterType
{	struct DTStructureGetTime GetData;
	struct TON TON_SaveDepend;
	struct TON TON_MaxSaveDelay;
	FileSupportInterFileNameSpptTyp FileNameSupport;
	FileSupportInterFileTyp FileSupport;
	unsigned long Offset;
	DTStructure DTStruct;
	FileSupportBufforDataTyp FileSupportBufforData;
	plcbit Init;
	plcstring Header[301];
} FileSupportInterType;
#endif

#ifndef __AS__TYPE_DetUsbInpCmdTyp
#define __AS__TYPE_DetUsbInpCmdTyp
typedef struct DetUsbInpCmdTyp
{	plcbit Enable;
	plcbit Reset;
	plcbit Stop;
	plcbit ForceQuickDet;
} DetUsbInpCmdTyp;
#endif

#ifndef __AS__TYPE_DetUsbInpParTyp
#define __AS__TYPE_DetUsbInpParTyp
typedef struct DetUsbInpParTyp
{	plctime AutoDetectionTime;
} DetUsbInpParTyp;
#endif

#ifndef __AS__TYPE_DetUsbInpTyp
#define __AS__TYPE_DetUsbInpTyp
typedef struct DetUsbInpTyp
{	DetUsbInpCmdTyp Cmd;
	DetUsbInpParTyp Params;
} DetUsbInpTyp;
#endif

#ifndef __AS__TYPE_USB_DET_SM
#define __AS__TYPE_USB_DET_SM
typedef enum USB_DET_SM
{	USB_DET_INIT = 0,
	USB_DET_GET_NODE_LIST = 1,
	USB_DET_GET_NODE_INFO = 2,
	USB_DET_DELETE_DEV_LINK = 3,
	USB_DET_QUICK_DETECTION = 4,
	USB_DET_CREATE_DEV_LINK = 5,
	USB_DET_WAIT = 6,
	USB_DET_ERROR = 7,
} USB_DET_SM;
#endif

#ifndef __AS__TYPE_DetUsbOutStatesTyp
#define __AS__TYPE_DetUsbOutStatesTyp
typedef struct DetUsbOutStatesTyp
{	USB_DET_SM SM_DET_USB;
} DetUsbOutStatesTyp;
#endif

#ifndef __AS__TYPE_DetUsbOutStatusInterTyp
#define __AS__TYPE_DetUsbOutStatusInterTyp
typedef struct DetUsbOutStatusInterTyp
{	unsigned char Index;
	unsigned char Time;
	signed short NumOfError;
} DetUsbOutStatusInterTyp;
#endif

#ifndef __AS__TYPE_DetUsbOutStatusTyp
#define __AS__TYPE_DetUsbOutStatusTyp
typedef struct DetUsbOutStatusTyp
{	unsigned long DevHandle;
	plcstring DevLink[10][51];
	plcbit DiscDetected;
	plcbit Error;
	unsigned char NumOfDev;
	DetUsbOutStatusInterTyp Internal;
} DetUsbOutStatusTyp;
#endif

#ifndef __AS__TYPE_DetUsbOutTyp
#define __AS__TYPE_DetUsbOutTyp
typedef struct DetUsbOutTyp
{	DetUsbOutStatesTyp States;
	DetUsbOutStatusTyp Status;
} DetUsbOutTyp;
#endif

#ifndef __AS__TYPE_DetUsbType
#define __AS__TYPE_DetUsbType
typedef struct DetUsbType
{	DetUsbInpTyp Input;
	DetUsbOutTyp Output;
} DetUsbType;
#endif

#ifndef __AS__TYPE_StringSupportInpCmdTyp
#define __AS__TYPE_StringSupportInpCmdTyp
typedef struct StringSupportInpCmdTyp
{	plcbit NewValue;
} StringSupportInpCmdTyp;
#endif

#ifndef __AS__TYPE_StringSupportInpParTyp
#define __AS__TYPE_StringSupportInpParTyp
typedef struct StringSupportInpParTyp
{	unsigned short MaxBuforLen;
	float Samples[41];
	unsigned char NumOfSamples;
} StringSupportInpParTyp;
#endif

#ifndef __AS__TYPE_StringSupportInpTyp
#define __AS__TYPE_StringSupportInpTyp
typedef struct StringSupportInpTyp
{	StringSupportInpCmdTyp Cmd;
	StringSupportInpParTyp Params;
} StringSupportInpTyp;
#endif

#ifndef __AS__TYPE_SM_STRING_SUPPORT
#define __AS__TYPE_SM_STRING_SUPPORT
typedef enum SM_STRING_SUPPORT
{	SM_STRING_WAIT_FOR_ENABLE = 0,
	SM_STRING_PREPARE_SAMPLES = 1,
} SM_STRING_SUPPORT;
#endif

#ifndef __AS__TYPE_StringSupportOutStatesTyp
#define __AS__TYPE_StringSupportOutStatesTyp
typedef struct StringSupportOutStatesTyp
{	SM_STRING_SUPPORT SM_STRING_SUPPORT;
} StringSupportOutStatesTyp;
#endif

#ifndef __AS__TYPE_StringSupportOutStatusTyp
#define __AS__TYPE_StringSupportOutStatusTyp
typedef struct StringSupportOutStatusTyp
{	plcbit ReadyToSave;
	plcstring StringToSave[10001];
	plcbit Error;
	unsigned short FillVector;
} StringSupportOutStatusTyp;
#endif

#ifndef __AS__TYPE_StringSupportOutTyp
#define __AS__TYPE_StringSupportOutTyp
typedef struct StringSupportOutTyp
{	StringSupportOutStatesTyp States;
	StringSupportOutStatusTyp Status;
} StringSupportOutTyp;
#endif

#ifndef __AS__TYPE_StringSupportType
#define __AS__TYPE_StringSupportType
typedef struct StringSupportType
{	StringSupportInpTyp Input;
	StringSupportOutTyp Output;
} StringSupportType;
#endif

#ifndef __AS__TYPE_FileSupportInpCmdTyp
#define __AS__TYPE_FileSupportInpCmdTyp
typedef struct FileSupportInpCmdTyp
{	plcbit ExitPen;
	plcbit TimeDependency;
} FileSupportInpCmdTyp;
#endif

#ifndef __AS__TYPE_FileSupportInpParTyp
#define __AS__TYPE_FileSupportInpParTyp
typedef struct FileSupportInpParTyp
{	plcstring NewFileName[81];
	unsigned short SaveTime;
} FileSupportInpParTyp;
#endif

#ifndef __AS__TYPE_FileSupportInpTyp
#define __AS__TYPE_FileSupportInpTyp
typedef struct FileSupportInpTyp
{	FileSupportInpCmdTyp Cmd;
	FileSupportInpParTyp Params;
} FileSupportInpTyp;
#endif

#ifndef __AS__TYPE_SM_FILE_PLC
#define __AS__TYPE_SM_FILE_PLC
typedef enum SM_FILE_PLC
{	SM_FILE_PLC_WAIT_FOR_ENABLE = 0,
	SM_FILE_PLC_CHECK_DATA = 1,
	SM_FILE_PLC_CHECK_FILE = 2,
	SM_FILE_PLC_OPEN = 3,
	SM_FILE_PLC_READ = 4,
	SM_FILE_PLC_WRITE = 5,
	SM_FILE_PLC_CLOSE = 6,
	SM_FILE_PLC_WAIT = 7,
	SM_FILE_PLC_ERROR = 8,
	SM_FILE_PLC_DELETE = 9,
	SM_FILE_PLC_CREATE = 10,
	SM_FILE_PLC_HEADER = 11,
} SM_FILE_PLC;
#endif

#ifndef __AS__TYPE_SM_CHECK_MONTH
#define __AS__TYPE_SM_CHECK_MONTH
typedef enum SM_CHECK_MONTH
{	SM_CHM_WAIT_FOR_ENABLE = 0,
	SM_CHM_READ_DATE = 1,
	SM_CHM_ERROR = 2,
} SM_CHECK_MONTH;
#endif

#ifndef __AS__TYPE_SM_DETECT_RDY_DATA
#define __AS__TYPE_SM_DETECT_RDY_DATA
typedef enum SM_DETECT_RDY_DATA
{	SM_DRD_WAIT = 0,
	SM_DRD_CHECK_LEN = 1,
} SM_DETECT_RDY_DATA;
#endif

#ifndef __AS__TYPE_FileSupportOutStatesTyp
#define __AS__TYPE_FileSupportOutStatesTyp
typedef struct FileSupportOutStatesTyp
{	SM_FILE_PLC SM_FILE_PLC;
	SM_CHECK_MONTH SM_FILE_MONTH;
	SM_DETECT_RDY_DATA SM_FILE_RDY_DATA;
} FileSupportOutStatesTyp;
#endif

#ifndef __AS__TYPE_FileSupportOutStatusTyp
#define __AS__TYPE_FileSupportOutStatusTyp
typedef struct FileSupportOutStatusTyp
{	plcbit Error;
	plcbit Sended;
	plcstring ActualFileName[81];
	plcbit PendriveClosed;
	unsigned long NumOfSave;
	unsigned char FullVectors;
} FileSupportOutStatusTyp;
#endif

#ifndef __AS__TYPE_FileSupportOutTyp
#define __AS__TYPE_FileSupportOutTyp
typedef struct FileSupportOutTyp
{	FileSupportOutStatesTyp States;
	FileSupportOutStatusTyp Status;
} FileSupportOutTyp;
#endif

#ifndef __AS__TYPE_FileSupportType
#define __AS__TYPE_FileSupportType
typedef struct FileSupportType
{	FileSupportInpTyp Input;
	FileSupportOutTyp Output;
} FileSupportType;
#endif

_BUR_PUBLIC plcstring* usint2str(unsigned char IN, plcstring pStr[81], unsigned long len);
_BUR_PUBLIC plcstring* uint2str(unsigned short IN, plcstring pStr[81], unsigned long len);
_BUR_PUBLIC unsigned long memset(unsigned long pDest, unsigned char value, unsigned long length);
_BUR_PUBLIC unsigned long strcat(unsigned long pDest, unsigned long pSrc);
_BUR_PUBLIC unsigned short strlen(unsigned long pString);
_BUR_LOCAL FileSupportInterType lFileSupport;
_GLOBAL DetUsbType gDetUsb;
_GLOBAL StringSupportType gStringSupport;
_GLOBAL FileSupportType gFileSupport;
_GLOBAL unsigned short ERR_OK;
_GLOBAL unsigned short ERR_FUB_BUSY;
_GLOBAL unsigned char fiREAD_WRITE;
_GLOBAL unsigned short fiERR_FILE_NOT_FOUND;
static void __AS__Action__IndexShift(void);
