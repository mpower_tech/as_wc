export AS_SYSTEM_PATH := C:/BrAutomation/AS/System
export AS_BIN_PATH := C:/BrAutomation/AS48/bin-en
export AS_INSTALL_PATH := C:/BrAutomation/AS48
export AS_PATH := C:/BrAutomation/AS48
export AS_VC_PATH := C:/BrAutomation/AS48/AS/VC
export AS_GNU_INST_PATH := C:/BrAutomation/AS48/AS/GnuInst/V4.1.2
export AS_STATIC_ARCHIVES_PATH := C:/Projects/As_Wc/Temp/Archives/Config1/4PPC30_043F_23B
export AS_CPU_PATH := C:/Projects/As_Wc/Temp/Objects/Config1/4PPC30_043F_23B
export AS_CPU_PATH_2 := C:/Projects/As_Wc/Temp/Objects/Config1/4PPC30_043F_23B
export AS_TEMP_PATH := C:/Projects/As_Wc/Temp
export AS_BINARIES_PATH := C:/Projects/As_Wc/Binaries
export AS_PROJECT_CPU_PATH := C:/Projects/As_Wc/Physical/Config1/4PPC30_043F_23B
export AS_PROJECT_CONFIG_PATH := C:/Projects/As_Wc/Physical/Config1
export AS_PROJECT_PATH := C:/Projects/As_Wc
export AS_PROJECT_NAME := system_chlodzenia
export AS_PLC := 4PPC30_043F_23B
export AS_TEMP_PLC := 4PPC30_043F_23B
export AS_USER_NAME := B.\ Kucharczyk
export AS_CONFIGURATION := Config1
export AS_COMPANY_NAME := \ 
export AS_VERSION := 4.8.2.72


default: \
	$(AS_CPU_PATH)/Visu.br \
	vcPostBuild_Visu \



include $(AS_CPU_PATH)/Visu/Visu.mak
