#ifndef __AS__TYPE_DTStructure
#define __AS__TYPE_DTStructure
typedef struct DTStructure
{	unsigned short year;
	unsigned char month;
	unsigned char day;
	unsigned char wday;
	unsigned char hour;
	unsigned char minute;
	unsigned char second;
	unsigned short millisec;
	unsigned short microsec;
} DTStructure;
#endif

#ifndef __AS__TYPE_StringSupportInterDataStringTyp
#define __AS__TYPE_StringSupportInterDataStringTyp
typedef struct StringSupportInterDataStringTyp
{	plcstring sec[3];
	plcstring minute[3];
	plcstring hour[3];
	plcstring day[3];
	plcstring year[5];
	plcstring month[3];
} StringSupportInterDataStringTyp;
#endif

struct DTStructureGetTime
{	unsigned long pDTStructure;
	unsigned short status;
	plcbit enable;
};
_BUR_PUBLIC void DTStructureGetTime(struct DTStructureGetTime* inst);
#ifndef __AS__TYPE_StringSupportInterDataTyp
#define __AS__TYPE_StringSupportInterDataTyp
typedef struct StringSupportInterDataTyp
{	DTStructure DTStruct;
	StringSupportInterDataStringTyp StringData;
	struct DTStructureGetTime GetData;
} StringSupportInterDataTyp;
#endif

#ifndef __AS__TYPE_StringSupportInterType
#define __AS__TYPE_StringSupportInterType
typedef struct StringSupportInterType
{	StringSupportInterDataTyp DataATime;
	plcstring DataToSave[201];
	unsigned short LenStringToSave;
	unsigned short LenDataToSave;
	plcstring ConvSample[101];
} StringSupportInterType;
#endif

_BUR_LOCAL StringSupportInterType lStringSupport;
