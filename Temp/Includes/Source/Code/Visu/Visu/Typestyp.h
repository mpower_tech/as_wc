/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1607521166_1_
#define _BUR_1607521166_1_

#include <bur/plctypes.h>

/* Datatypes and datatypes of function blocks */
typedef struct VisuLayersTyp
{	signed short Pumps;
	signed short Heater;
} VisuLayersTyp;

typedef struct VisuBtnTyp
{	signed short PumpTurnOn;
	signed short CoolerMachine;
	signed short Heater;
	signed short ConstValueStart;
} VisuBtnTyp;

typedef struct VisuScaleTyp
{	float thermalPowerScale;
	float flowScale;
	float frequencyScale;
	float tempScale;
	float tempMinScale;
} VisuScaleTyp;

typedef struct VisuNumericTyp
{	signed short PercVoltage;
	signed short FlowSet;
} VisuNumericTyp;

typedef struct VisuBlokadeTyp
{	signed short Auto;
	signed short Manual;
} VisuBlokadeTyp;

typedef struct VisuTyp
{	unsigned char Dialog;
	struct VisuLayersTyp Layers;
	struct VisuBtnTyp Button;
	struct VisuScaleTyp Scale;
	struct VisuNumericTyp Numeric;
	struct VisuBlokadeTyp Blokade;
} VisuTyp;






__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Visu/Visu/Types.typ\\\" scope \\\"local\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1607521166_1_ */

