/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1606123496_10_
#define _BUR_1606123496_10_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_BUR_LOCAL struct MTFilterMovingAverage MovingAverageFiowMeter;
_BUR_LOCAL unsigned short step;
_BUR_LOCAL plcbit start;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Sensors/Sensors/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTFilter/MTFilter.fun\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1606123496_10_ */

