/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1606123496_4_
#define _BUR_1606123496_4_

#include <bur/plctypes.h>

/* Datatypes and datatypes of function blocks */
typedef enum USB_DET_SM
{	USB_DET_INIT,
	USB_DET_GET_NODE_LIST,
	USB_DET_GET_NODE_INFO,
	USB_DET_DELETE_DEV_LINK,
	USB_DET_QUICK_DETECTION,
	USB_DET_CREATE_DEV_LINK,
	USB_DET_WAIT,
	USB_DET_ERROR
} USB_DET_SM;

typedef struct DetUsbInpCmdTyp
{	plcbit Enable;
	plcbit Reset;
	plcbit Stop;
	plcbit ForceQuickDet;
} DetUsbInpCmdTyp;

typedef struct DetUsbInpParTyp
{	plctime AutoDetectionTime;
} DetUsbInpParTyp;

typedef struct DetUsbInpTyp
{	struct DetUsbInpCmdTyp Cmd;
	struct DetUsbInpParTyp Params;
} DetUsbInpTyp;

typedef struct DetUsbOutStatesTyp
{	enum USB_DET_SM SM_DET_USB;
} DetUsbOutStatesTyp;

typedef struct DetUsbOutStatusInterTyp
{	unsigned char Index;
	unsigned char Time;
	signed short NumOfError;
} DetUsbOutStatusInterTyp;

typedef struct DetUsbOutStatusTyp
{	unsigned long DevHandle;
	plcstring DevLink[10][51];
	plcbit DiscDetected;
	plcbit Error;
	unsigned char NumOfDev;
	struct DetUsbOutStatusInterTyp Internal;
} DetUsbOutStatusTyp;

typedef struct DetUsbOutTyp
{	struct DetUsbOutStatesTyp States;
	struct DetUsbOutStatusTyp Status;
} DetUsbOutTyp;

typedef struct DetUsbType
{	struct DetUsbInpTyp Input;
	struct DetUsbOutTyp Output;
} DetUsbType;






__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/Detection/gDetUsbTyp.typ\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1606123496_4_ */

