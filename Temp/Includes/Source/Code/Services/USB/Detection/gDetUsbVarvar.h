/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1606123496_3_
#define _BUR_1606123496_3_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_GLOBAL struct DetUsbType gDetUsb;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/Detection/gDetUsbVar.var\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1606123496_3_ */

