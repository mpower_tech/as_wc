/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1606123496_12_
#define _BUR_1606123496_12_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_BUR_LOCAL struct DetUsbInternalType lDetUsbInternal;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/Detection/Detection/Variables.var\\\" scope \\\"local\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1606123496_12_ */

