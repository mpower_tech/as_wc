/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1606123496_11_
#define _BUR_1606123496_11_

#include <bur/plctypes.h>

/* Datatypes and datatypes of function blocks */
typedef struct DetUsbInterDataInterTyp
{	plcstring pParam[81];
	unsigned long NodeIfBuffer[10];
	struct usbNode_typ UsbData;
} DetUsbInterDataInterTyp;

typedef struct DetUsbInternalType
{	struct DetUsbInterDataInterTyp DataInternal;
	struct UsbNodeGet UsbNodeGet;
	struct DevUnlink DevUnlink;
	struct DevLink DevLink;
	struct UsbMsDeviceReady UsbMsDeviceReady;
	struct UsbNodeListGet UsbNodeListGet;
	struct TON Redetection;
} DetUsbInternalType;






__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/Detection/Detection/Types.typ\\\" scope \\\"local\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1606123496_11_ */

