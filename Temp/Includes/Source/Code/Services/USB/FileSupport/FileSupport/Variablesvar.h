/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1606123496_16_
#define _BUR_1606123496_16_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_BUR_LOCAL struct FileSupportInterType lFileSupport;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/FileSupport/FileSupport/Variables.var\\\" scope \\\"local\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1606123496_16_ */

