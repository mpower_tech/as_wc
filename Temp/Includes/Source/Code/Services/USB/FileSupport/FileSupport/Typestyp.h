/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1606123496_15_
#define _BUR_1606123496_15_

#include <bur/plctypes.h>

/* Datatypes and datatypes of function blocks */
typedef struct FileSupportInterFileNameSpptTyp
{	plcstring Year[5];
	plcstring Month[3];
	plcstring InternalFileName[81];
} FileSupportInterFileNameSpptTyp;

typedef struct FileSupportInterFileTyp
{	struct FileWrite FileWrite;
	struct FileOpen FileOpen;
	struct FileClose FileClose;
	struct FileCreate FileCreate;
} FileSupportInterFileTyp;

typedef struct FileSupportBufforDataTyp
{	plcstring Buffor[3][10001];
	unsigned char Index;
	plcstring FileNameIndex[3][81];
} FileSupportBufforDataTyp;

typedef struct FileSupportInterType
{	struct DTStructureGetTime GetData;
	struct TON TON_SaveDepend;
	struct TON TON_MaxSaveDelay;
	struct FileSupportInterFileNameSpptTyp FileNameSupport;
	struct FileSupportInterFileTyp FileSupport;
	unsigned long Offset;
	struct DTStructure DTStruct;
	struct FileSupportBufforDataTyp FileSupportBufforData;
	plcbit Init;
	plcstring Header[301];
} FileSupportInterType;






__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/FileSupport/FileSupport/Types.typ\\\" scope \\\"local\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1606123496_15_ */

