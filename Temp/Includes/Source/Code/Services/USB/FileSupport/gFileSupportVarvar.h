/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1606123496_8_
#define _BUR_1606123496_8_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_GLOBAL struct FileSupportType gFileSupport;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/FileSupport/gFileSupportVar.var\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1606123496_8_ */

