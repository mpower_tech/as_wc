/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1606123496_7_
#define _BUR_1606123496_7_

#include <bur/plctypes.h>

/* Datatypes and datatypes of function blocks */
typedef enum SM_FILE_PLC
{	SM_FILE_PLC_WAIT_FOR_ENABLE,
	SM_FILE_PLC_CHECK_DATA,
	SM_FILE_PLC_CHECK_FILE,
	SM_FILE_PLC_OPEN,
	SM_FILE_PLC_READ,
	SM_FILE_PLC_WRITE,
	SM_FILE_PLC_CLOSE,
	SM_FILE_PLC_WAIT,
	SM_FILE_PLC_ERROR,
	SM_FILE_PLC_DELETE,
	SM_FILE_PLC_CREATE,
	SM_FILE_PLC_HEADER
} SM_FILE_PLC;

typedef enum SM_CHECK_MONTH
{	SM_CHM_WAIT_FOR_ENABLE,
	SM_CHM_READ_DATE,
	SM_CHM_ERROR
} SM_CHECK_MONTH;

typedef enum SM_DETECT_RDY_DATA
{	SM_DRD_WAIT,
	SM_DRD_CHECK_LEN
} SM_DETECT_RDY_DATA;

typedef struct FileSupportInpCmdTyp
{	plcbit ExitPen;
	plcbit TimeDependency;
} FileSupportInpCmdTyp;

typedef struct FileSupportInpParTyp
{	plcstring NewFileName[81];
	unsigned short SaveTime;
} FileSupportInpParTyp;

typedef struct FileSupportInpTyp
{	struct FileSupportInpCmdTyp Cmd;
	struct FileSupportInpParTyp Params;
} FileSupportInpTyp;

typedef struct FileSupportOutStatesTyp
{	enum SM_FILE_PLC SM_FILE_PLC;
	enum SM_CHECK_MONTH SM_FILE_MONTH;
	enum SM_DETECT_RDY_DATA SM_FILE_RDY_DATA;
} FileSupportOutStatesTyp;

typedef struct FileSupportOutStatusTyp
{	plcbit Error;
	plcbit Sended;
	plcstring ActualFileName[81];
	plcbit PendriveClosed;
	unsigned long NumOfSave;
	unsigned char FullVectors;
} FileSupportOutStatusTyp;

typedef struct FileSupportOutTyp
{	struct FileSupportOutStatesTyp States;
	struct FileSupportOutStatusTyp Status;
} FileSupportOutTyp;

typedef struct FileSupportType
{	struct FileSupportInpTyp Input;
	struct FileSupportOutTyp Output;
} FileSupportType;






__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/FileSupport/gFileSupportTyp.typ\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1606123496_7_ */

