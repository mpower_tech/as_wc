/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1606123496_14_
#define _BUR_1606123496_14_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_BUR_LOCAL struct StringSupportInterType lStringSupport;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/StringCreator/StringSupport/Variables.var\\\" scope \\\"local\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1606123496_14_ */

