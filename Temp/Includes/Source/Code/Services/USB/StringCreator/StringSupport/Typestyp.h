/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1606123496_13_
#define _BUR_1606123496_13_

#include <bur/plctypes.h>

/* Datatypes and datatypes of function blocks */
typedef struct StringSupportInterDataStringTyp
{	plcstring sec[3];
	plcstring minute[3];
	plcstring hour[3];
	plcstring day[3];
	plcstring year[5];
	plcstring month[3];
} StringSupportInterDataStringTyp;

typedef struct StringSupportInterDataTyp
{	struct DTStructure DTStruct;
	struct StringSupportInterDataStringTyp StringData;
	struct DTStructureGetTime GetData;
} StringSupportInterDataTyp;

typedef struct StringSupportInterType
{	struct StringSupportInterDataTyp DataATime;
	plcstring DataToSave[201];
	unsigned short LenStringToSave;
	unsigned short LenDataToSave;
	plcstring ConvSample[101];
} StringSupportInterType;






__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/StringCreator/StringSupport/Types.typ\\\" scope \\\"local\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1606123496_13_ */

