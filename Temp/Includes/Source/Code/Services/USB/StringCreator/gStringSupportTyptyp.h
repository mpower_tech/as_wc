/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1606123496_5_
#define _BUR_1606123496_5_

#include <bur/plctypes.h>

/* Datatypes and datatypes of function blocks */
typedef enum SM_STRING_SUPPORT
{	SM_STRING_WAIT_FOR_ENABLE,
	SM_STRING_PREPARE_SAMPLES
} SM_STRING_SUPPORT;

typedef struct StringSupportInpCmdTyp
{	plcbit NewValue;
} StringSupportInpCmdTyp;

typedef struct StringSupportInpParTyp
{	unsigned short MaxBuforLen;
	float Samples[41];
	unsigned char NumOfSamples;
} StringSupportInpParTyp;

typedef struct StringSupportInpTyp
{	struct StringSupportInpCmdTyp Cmd;
	struct StringSupportInpParTyp Params;
} StringSupportInpTyp;

typedef struct StringSupportOutStatesTyp
{	enum SM_STRING_SUPPORT SM_STRING_SUPPORT;
} StringSupportOutStatesTyp;

typedef struct StringSupportOutStatusTyp
{	plcbit ReadyToSave;
	plcstring StringToSave[10001];
	plcbit Error;
	unsigned short FillVector;
} StringSupportOutStatusTyp;

typedef struct StringSupportOutTyp
{	struct StringSupportOutStatesTyp States;
	struct StringSupportOutStatusTyp Status;
} StringSupportOutTyp;

typedef struct StringSupportType
{	struct StringSupportInpTyp Input;
	struct StringSupportOutTyp Output;
} StringSupportType;






__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Source/Code/Services/USB/StringCreator/gStringSupportTyp.typ\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1606123496_5_ */

