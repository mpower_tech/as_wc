/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1606123496_1_
#define _BUR_1606123496_1_

#include <bur/plctypes.h>

/* Datatypes and datatypes of function blocks */
typedef struct ExtDevTempTyp
{	plcbit state;
	float temp;
	signed long temp0x;
	unsigned char mainState;
} ExtDevTempTyp;

typedef struct ExtTempmeterTyp
{	struct ExtDevTempTyp Inlet;
	struct ExtDevTempTyp Outlet;
	struct ExtDevTempTyp Tank;
} ExtTempmeterTyp;

typedef struct ExtDevFlowmeterTyp
{	unsigned short pulses;
	float flow;
	float frequency;
} ExtDevFlowmeterTyp;

typedef struct ExtDevWaterpumpTyp
{	unsigned char mode;
	plcbit start;
	unsigned char setPerc;
	float setFlow;
	signed short analogOutput;
	float voltage;
} ExtDevWaterpumpTyp;

typedef struct ExtDevModulesTyp
{	plcbit ATB312;
	plcbit CM8281;
	plcbit AO4622;
} ExtDevModulesTyp;

typedef struct ExtDevModbusOutTyp
{	unsigned short actFlow;
	unsigned short powerThermal;
	unsigned short tempInlet;
	unsigned short tempOutlet;
	unsigned short tempTank;
} ExtDevModbusOutTyp;

typedef struct ExtPowerThermalTyp
{	float powerTerm;
} ExtPowerThermalTyp;

typedef struct ExtDevTempControlTyp
{	float setTankTemp;
	plcbit start;
	float histeresis;
	unsigned char SM;
} ExtDevTempControlTyp;

typedef struct ExtDevType
{	struct ExtTempmeterTyp TempMeter;
	struct ExtDevFlowmeterTyp Flowmeter;
	struct ExtDevWaterpumpTyp WaterPump;
	plcbit WaterPumpCoolerAndFan;
	plcbit Heater;
	struct ExtDevModulesTyp Modules;
	struct ExtDevModbusOutTyp ModbusOut;
	struct ExtPowerThermalTyp PowerThermal;
	struct ExtDevTempControlTyp TempControl;
} ExtDevType;






__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1606123496_1_ */

