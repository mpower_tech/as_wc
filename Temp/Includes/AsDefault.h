#ifndef _DEFAULT_757602046
#define _DEFAULT_757602046
#include <bur/plctypes.h>
#include <bur/plc.h>

#ifdef __cplusplus 
extern "C" 
{
#endif
	#include <operator.h>
	#include <runtime.h>
	#include <asstring.h>
	#include <standard.h>
	#include <MTTypes.h>
	#include <astime.h>
	#include <FileIO.h>
	#include <AsUSB.h>
	#include <brsystem.h>
	#include <sys_lib.h>
	#include <AsIecCon.h>
	#include <MTBasics.h>
	#include <MTFilter.h>
#ifdef __cplusplus
};
#endif

#include <globalTYP.h>
#include <globalVAR.h>
#include <source/code/services/usb/detection/gdetusbvarVAR.h>
#include <source/code/services/usb/detection/gdetusbtypTYP.h>
#include <source/code/services/usb/stringcreator/gstringsupporttypTYP.h>
#include <source/code/services/usb/stringcreator/gstringsupportvarVAR.h>
#include <source/code/services/usb/filesupport/gfilesupporttypTYP.h>
#include <source/code/services/usb/filesupport/gfilesupportvarVAR.h>
#endif
